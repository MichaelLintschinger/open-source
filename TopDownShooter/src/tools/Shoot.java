package tools;

public class Shoot {
	int x;
	int y;
	int startX;
	int startY;
	double angle;
	int calctimes;

	public Shoot(int x, int y, double angle) {
		startX = x;
		startY = y;
		this.angle = angle;
		calctimes = 0;
	}

	public void setX(int a) {
		x = a;
	}

	public void setY(int a) {
		y = a;
	}

	public void setA(int a) {
		angle = a;
	}

	public void addTimes() {
		calctimes++;
	}

	public int getX() {

		return x;
	}

	public int getY() {
		return y;
	}

	public double getAngle() {
		return angle;
	}

	public int getTimes() {
		return calctimes;
	}

	public int getStartX() {
		return startX;
	}

	public int getStartY() {
		return startY;
	}

}
