package enemy.enemyX;

import graphics.*;
import tools.*;

import java.awt.*;
import java.util.*;
import java.util.concurrent.*;
import javax.swing.JFrame;

public class EnemyXMethods extends Thread {

	int counterAdd = 0;
	int addEnemy = 600;
	int shipSize = 50;

	/* parameter */
	static ArrayList<EnemyX> enemyX;

	public EnemyXMethods() {

	}

	@SuppressWarnings("static-access")
	public ArrayList<EnemyX> carryEnemyX(ArrayList<EnemyX> enemyA, int enemyA_size, Image enemyX_ship, Graphics2D g,
			Pos2 shipData, int width, int height) {

		this.enemyX = enemyA;

		enemyX = add(enemyX, width);

		for (int i = 0; i < enemyA.size(); i++) {
			enemyX.set(i, move(enemyX.get(i), height));
			enemyX.set(i, shoot(enemyX.get(i), shipData.getX(), shipData.getY(), width, height));
			draw(g, enemyX.get(i), enemyA_size, enemyX_ship);

		}

		for (int i = 0; i < enemyX.size(); i++) {
			if (enemyX.get(i).getY() > height) {
				enemyX.remove(i);
			}
		}

		return enemyX;
	}

	private EnemyX shoot(EnemyX enemyX, int shipX, int shipY, int width, int height) {
		enemyX.setShootCounter(enemyX.getShootCounter() + 1);
		if (enemyX.getShootCounter() >= 200) {
			enemyX.setShootCounter(0);
			enemyX.addShot(new Shoot(enemyX.getX(), enemyX.getY(), -Math.PI / 4));
			enemyX.addShot(new Shoot(enemyX.getX(), enemyX.getY(), Math.PI / 4));

			enemyX.addShot(new Shoot(enemyX.getX(), enemyX.getY(), Math.PI - Math.PI / 4));
			enemyX.addShot(new Shoot(enemyX.getX(), enemyX.getY(), Math.PI + Math.PI / 4));

		}

		int r = 2;
		double x;
		double y;

		for (int j = 0; j < enemyX.getShots().size(); j++) {
			x = enemyX.getShots().get(j).getStartX()
					+ enemyX.getShots().get(j).getTimes() * r * Math.cos(enemyX.getShots().get(j).getAngle());
			y = enemyX.getShots().get(j).getStartY()
					+ enemyX.getShots().get(j).getTimes() * r * Math.sin(enemyX.getShots().get(j).getAngle());

			enemyX.getShots().get(j).addTimes();

			enemyX.getShots().get(j).setX((int) (x));
			enemyX.getShots().get(j).setY((int) (y));

		}

		return enemyX;
	}

	private ArrayList<EnemyX> add(ArrayList<EnemyX> enemyA, int width) {
		counterAdd++;

		if (counterAdd >= addEnemy) {
			if (addEnemy > 100) {
				addEnemy -= 10;
			}
			counterAdd = 0;

			enemyA.add(new EnemyX(ThreadLocalRandom.current().nextInt(20, width - 110), 30));

		}

		return enemyA;
	}

	private EnemyX move(EnemyX enemyA, int height) {

		if (enemyA.getSpeed() == 1) {
			enemyA.setSpeed(0);
		} else {
			enemyA.setSpeed(1);
		}

		enemyA.setY(enemyA.getY() + enemyA.getSpeed());

		return enemyA;
	}

	private void draw(Graphics2D g, EnemyX enemyA, int enemyA_size, Image enemyX_ship) {

		GraphicContainer m = new GraphicContainer(g);

		for (int j = 0; j < enemyA.getShotSize(); j++) {
			m.drawBall(enemyA.getShot(j).getX(), enemyA.getShot(j).getY(), 3, Color.yellow);
		}
		enemyX_ship = enemyX_ship.getScaledInstance(enemyA_size, enemyA_size, Image.SCALE_SMOOTH);

		g.drawImage(enemyX_ship, enemyA.getX() - enemyA_size / 2, enemyA.getY() - enemyA_size / 2, new JFrame());

	}

}
