package enemy.enemyS;

import graphics.*;
import tools.*;

import java.awt.*;
import java.util.*;
import java.util.concurrent.*;

import javax.swing.JFrame;

public class EnemySMethods {

	int counterAdd = 0, counterMoove = 0, addEnemy = 200, index;
	final int shipSize = 50, addMin = 50, speed = 1;

	/* parameter */
	static ArrayList<EnemyS> enemySimple;

	public EnemySMethods(int index) {
		this.index = index;
	}

	public EnemySMethods() {

	}

	@SuppressWarnings("static-access")
	public ArrayList<EnemyS> carrySEnemy(ArrayList<EnemyS> enemyS, int enemyS_size, Image ship, Graphics2D g,
			Pos2 shipData, int width, int height) {

		this.enemySimple = enemyS;

		enemyS = add(enemyS, width);

		for (int i = 0; i < enemyS.size(); i++) {
			index = i;
			enemyS.set(index, move(enemyS.get(index), height));
			enemyS.set(index, shoot(enemyS.get(index), shipData.getX(), shipData.getY(), width, height));
			draw(g, enemyS.get(index), enemyS_size, ship);

		}

		for (int i = 0; i < enemyS.size(); i++) {
			if (enemyS.get(i).getY() > height) {
				enemyS.remove(i);
			}
		}

		return enemyS;
	}

	private EnemyS shoot(EnemyS enemyS, int shipX, int shipY, int width, int height) {
		enemyS.setShootCounter(enemyS.getShootCounter() + 1);
		if (enemyS.getShootCounter() >= 100) {
			enemyS.setShootCounter(0);
			enemyS.addShot(new Shoot(enemyS.getX(), enemyS.getY(), Math.PI / 2));
		}

		int r = 2;
		double x;
		double y;

		for (int j = 0; j < enemyS.getShots().size(); j++) {
			x = enemyS.getShots().get(j).getStartX()
					+ enemyS.getShots().get(j).getTimes() * r * Math.cos(enemyS.getShots().get(j).getAngle());
			y = enemyS.getShots().get(j).getStartY()
					+ enemyS.getShots().get(j).getTimes() * r * Math.sin(enemyS.getShots().get(j).getAngle());

			enemyS.getShots().get(j).addTimes();

			enemyS.getShots().get(j).setX((int) (x));
			enemyS.getShots().get(j).setY((int) (y));

		}

		return enemyS;
	}

	private ArrayList<EnemyS> add(ArrayList<EnemyS> enemyS, int width) {
		counterAdd++;

		if (counterAdd >= addEnemy) {
			if (addEnemy > addMin) {
				addEnemy -= 5;
			}
			counterAdd = 0;

			enemyS.add(new EnemyS(ThreadLocalRandom.current().nextInt(20, width - 110), 30));

		}

		return enemyS;
	}

	private EnemyS move(EnemyS enemyS, int height) {

		enemyS.setY(enemyS.getY() + speed);

		return enemyS;
	}

	private void draw(Graphics2D g, EnemyS enemyS, int enemyA_size, Image ship) {

		GraphicContainer m = new GraphicContainer(g);

		for (int j = 0; j < enemyS.getShotSize(); j++) {
			m.drawBall(enemyS.getShot(j).getX(), enemyS.getShot(j).getY(), 3, Color.red);
		}
		g.drawImage(ship, enemyS.getX() - enemyA_size / 2,
				enemyS.getY() - enemyA_size / 2 - (int) (0.17 * enemyA_size),
				new JFrame());

	}

}
