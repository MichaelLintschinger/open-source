package enemy.enemyS;

import java.util.ArrayList;
import tools.*;

public class EnemyS{

	int x;
	int y;
	int speed;
	int live;
	int shootCounter;

	ArrayList<Shoot> shots;

	public EnemyS(int x, int y) {
		shots = new ArrayList<Shoot>();
		this.x = x;
		this.y = y;

	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Shoot getShot(int i) {
		return shots.get(i);
	}

	public void setShot(int i, Shoot s) {
		shots.set(i, s);
	}

	public void addShot(Shoot s) {
		shots.add(s);
	}

	public int getShotSize() {
		return shots.size();
	}

	public void removeShot(int i) {
		shots.remove(i);
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	public int getLive() {
		return live;
	}

	public void setLive(int live) {
		this.live = live;
	}

	public ArrayList<Shoot> getShots() {
		return shots;
	}

	public void setShots(ArrayList<Shoot> shots) {
		this.shots = shots;
	}

	public int getShootCounter() {
		return shootCounter;
	}

	public void setShootCounter(int shootCounter) {
		this.shootCounter = shootCounter;
	}

}
