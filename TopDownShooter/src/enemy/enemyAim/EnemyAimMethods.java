package enemy.enemyAim;

import graphics.*;
import tools.*;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.util.*;
import java.util.concurrent.*;

import javax.swing.JFrame;

public class EnemyAimMethods extends Thread {

	int counterAdd = 0;
	int addEnemy = 800;
	int shipSize = 50;

	/* parameter */
	static ArrayList<EnemyAim> enemyAim;

	public EnemyAimMethods() {

	}

	@SuppressWarnings("static-access")
	public ArrayList<EnemyAim> carryAimEnemy(ArrayList<EnemyAim> enemyA, int enemyA_size, Image boddy, Image canon,
			Graphics2D g, Pos2 shipData, int width, int height) {

		this.enemyAim = enemyA;

		enemyAim = add(enemyAim, width);

		for (int i = 0; i < enemyA.size(); i++) {
			enemyAim.set(i, move(enemyAim.get(i), height));
			enemyAim.set(i, setAngle(enemyAim.get(i), shipData.getX(), shipData.getY()));
			enemyAim.set(i, shoot(enemyAim.get(i), shipData.getX(), shipData.getY(), width, height));
			draw(g, enemyAim.get(i), enemyA_size, boddy, canon);

		}

		for (int i = 0; i < enemyAim.size(); i++) {
			if (enemyAim.get(i).getY() > height) {
				enemyAim.remove(i);
			}
		}

		return enemyAim;
	}

	private EnemyAim shoot(EnemyAim enemyA, int shipX, int shipY, int width, int height) {
		enemyA.setShootCounter(enemyA.getShootCounter() + 1);
		if (enemyA.getShootCounter() >= 200) {
			enemyA.setShootCounter(0);
			enemyA.addShot(new Shoot(enemyA.getX(), enemyA.getY(), enemyA.getAngle()));
		}

		int r = 2;
		double x;
		double y;

		for (int j = 0; j < enemyA.getShots().size(); j++) {
			x = enemyA.getShots().get(j).getStartX()
					+ enemyA.getShots().get(j).getTimes() * r * Math.cos(enemyA.getShots().get(j).getAngle());
			y = enemyA.getShots().get(j).getStartY()
					+ enemyA.getShots().get(j).getTimes() * r * Math.sin(enemyA.getShots().get(j).getAngle());

			enemyA.getShots().get(j).addTimes();

			enemyA.getShots().get(j).setX((int) (x));
			enemyA.getShots().get(j).setY((int) (y));

		}

		return enemyA;
	}

	private ArrayList<EnemyAim> add(ArrayList<EnemyAim> enemyA, int width) {
		counterAdd++;

		if (counterAdd >= addEnemy) {
			if (addEnemy > 100) {
				addEnemy -= 10;
			}
			counterAdd = 0;

			enemyA.add(new EnemyAim(ThreadLocalRandom.current().nextInt(20, width - 110), 30));

		}

		return enemyA;
	}

	private EnemyAim move(EnemyAim enemyA, int height) {

		if (enemyA.getSpeed() == 1) {
			enemyA.setSpeed(0);
		} else {
			enemyA.setSpeed(1);
		}

		enemyA.setY(enemyA.getY() + enemyA.getSpeed());

		return enemyA;
	}

	private EnemyAim setAngle(EnemyAim enemyA, int shipX, int shipY) {

		int[] p1 = new int[2];
		int[] p2 = { shipX + shipSize / 2, shipY + shipSize / 2 };
		// iteration

		p1[0] = enemyA.getX();
		p1[1] = enemyA.getY();

		enemyA.setAngle(getAngle(p1, p2));

		return enemyA;
	}

	private void draw(Graphics2D g, EnemyAim enemyA, int enemyA_size, Image boddy, Image canon) {

		GraphicContainer m = new GraphicContainer(g);

		for (int j = 0; j < enemyA.getShotSize(); j++) {
			m.drawBall(enemyA.getShot(j).getX(), enemyA.getShot(j).getY(), 3, Color.MAGENTA);
		}
		g.drawImage(boddy, enemyA.getX() - enemyA_size / 2,
				enemyA.getY() - enemyA_size / 2 - (int) (0.17 * enemyA_size), new JFrame());
		
		AffineTransform at = new AffineTransform();
		at.setToRotation(enemyA.getAngle() - Math.PI / 2, enemyA.getX(),
				enemyA.getY() - 2 + (int) (4 * enemyA_size / 50));
		at.translate(enemyA.getX(), enemyA.getY() + (int) (4 * enemyA_size / 50));
		g.setTransform(at);
		g.drawImage(canon, enemyA_size / -2, enemyA_size / -2, new JFrame());
		at.setToRotation(0, enemyA.getX() + (canon.getWidth(null) / 2),
				enemyA.getY() + (canon.getHeight(null) / 2) + (int) (9 * enemyA_size / 50));
		g.setTransform(at);
		
	}

	private double getAngle(int[] p1, int[] p2) {
		double a = Math.PI / 2;

		double x = p2[0] - p1[0];
		double y = p1[1] - p2[1];
		double z = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

		if (x == 0) {
			if (y > 0) {
				a = Math.PI / -2;
			} else if (y < 0) {
				a = Math.PI / 2;
			}
		} else if (y == 0) {
			if (x > 0) {
				a = 0;
			} else if (x < 0) {
				a = Math.PI;
			}
		} else if (x > 0 && y > 0) {
			a = Math.acos(x / z) * -1;
		} else if (x < 0 && y > 0) {
			a = Math.acos(x / z) * -1;
		} else if (x < 0 && y < 0) {
			a = Math.acos(x / z);
		} else if (x > 0 && y < 0) {
			a = Math.asin(y / z) * -1;
		}
		// System.out.println(x + ", " + y + ", " + z);
		// System.out.println(a);

		return (a);
	}

}
