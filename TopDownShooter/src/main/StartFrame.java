package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class StartFrame extends JFrame {

	JPanel p;
	JButton btnStart, btnClose;

	public StartFrame() {
		setSize(300, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(getPanel());
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public static void main(String[] args) {
		new StartFrame();
	}

	public JPanel getPanel() {
		if (p == null) {
			p = new JPanel();
			p.setLayout(null);
			p.add(getBtnClose());
			p.add(getBtnStart());
		}
		return p;
	}

	public JButton getBtnStart() {
		if (btnStart == null) {
			btnStart = new JButton("Start");
			btnStart.setBounds(100, 70, 100, 30);
			btnStart.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					new StartShooter(true);
					close();
				}

			});
		}
		return btnStart;
	}

	public void close() {
		this.dispose();
	}

	public JButton getBtnClose() {
		if (btnClose == null) {
			btnClose = new JButton("Close");
			btnClose.setBounds(100, 140, 100, 30);
			btnClose.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					System.exit(0);
				}

			});

		}
		return btnClose;
	}

}
