package main;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;

import javax.imageio.*;
import javax.swing.*;

import graphics.*;
import tools.*;
import enemy.enemyAim.*;
import enemy.enemyS.*;
import enemy.enemyX.*;

@SuppressWarnings("serial")
public class RenderPanel extends JPanel {
	public boolean gameOver;
	boolean start, admin, play, shoot, enemyB, showHitbox, hit, prehit, bgchanged, mooveRIGHT, mooveUP, mooveLEFT,
			mooveDOWN;

	int enemySSpeed, counterES, enemySize, shipHP, counterGO, score, counterHit, counterShot, counterDiffEnemySAdd;
	final int enemyX_size = 100, enemyS_size = 100, enemyA_size = 100, shipSize = 50;

	int[] framePresize;

	GraphicContainer m;
	Pos2 shipData;
	ArrayList<Shoot> shots;

	ArrayList<EnemyAim> enemyA;
	EnemyAimMethods enemyAimMethods;

	ArrayList<EnemyS> enemyS;
	EnemySMethods enemySMethods;

	ArrayList<EnemyX> enemyX;
	EnemyXMethods enemyXMethods;

	JLabel lblScore;
	Image background, ship, enemyS_img, enemyA_boddy, enemyA_canon, enemyX_img;
	private JLabel lblAutor;
	private JLabel label;

	public RenderPanel(boolean admin) {
		this.admin = admin;
		gameOver = false;
		start = true;
		play = false;
		shoot = false;
		enemyB = true;
		showHitbox = false;
		hit = false;
		prehit = false;
		bgchanged = false;
		mooveDOWN = false;
		mooveUP = false;
		mooveLEFT = false;
		mooveRIGHT = false;

		enemySSpeed = 1;
		counterES = 0;
		enemySize = 15;
		shipHP = 100;
		counterGO = 0;
		score = 0;
		counterHit = 0;
		counterShot = 0;
		counterDiffEnemySAdd = 0;

		framePresize = new int[2];

		shipData = new Pos2();
		shots = new ArrayList<Shoot>();

		enemyS = new ArrayList<EnemyS>();
		enemySMethods = new EnemySMethods();

		enemyA = new ArrayList<EnemyAim>();
		enemyAimMethods = new EnemyAimMethods();

		enemyX = new ArrayList<EnemyX>();
		enemyXMethods = new EnemyXMethods();

		try {
			ship = ImageIO.read(new File("./resource/Ship.PNG"));
			background = ImageIO.read(new File("./resource/background.png"));

			enemyS_img = ImageIO.read(new File("./resource/enemyS.png")).getScaledInstance(enemyS_size, enemyS_size,
					Image.SCALE_SMOOTH);

			enemyA_boddy = ImageIO.read(new File("./resource/enemyA_boddy.png")).getScaledInstance(enemyA_size,
					enemyA_size, Image.SCALE_SMOOTH);
			enemyA_canon = ImageIO.read(new File("./resource/enemyA_canon.png")).getScaledInstance(enemyA_size,
					enemyA_size, Image.SCALE_SMOOTH);
			enemyX_img = ImageIO.read(new File("./resource/shipX.png")).getScaledInstance(enemyX_size, enemyX_size,
					Image.SCALE_SMOOTH);

		} catch (IOException e1) {
			System.out.println(e1);
		}

		lblScore = new JLabel("" + score);
		lblScore.setHorizontalAlignment(SwingConstants.RIGHT);
		lblScore.setForeground(Color.WHITE);
		lblScore.setBounds(732, 13, 56, 16);
		add(lblScore);

		setFocusable(true);
		setLayout(null);
		add(getLblAutor());
		add(getLabel());

		addKeyListener(new KeyListener() {

			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyChar()) {
				case 'w':
					mooveUP = true;
					break;
				case 'a':
					mooveLEFT = true;
					break;
				case 's':
					mooveDOWN = true;
					break;
				case 'd':
					mooveRIGHT = true;
					break;
				}

			}

			@Override
			public void keyReleased(KeyEvent e) {
				int ec = e.getKeyCode();
				// System.out.println(ec);
				if (ec == 87) {
					mooveUP = false;
				} else if (ec == 65) {
					mooveLEFT = false;
				} else if (ec == 83) {
					mooveDOWN = false;
				} else if (ec == 68) {
					mooveRIGHT = false;
				}

			}

			@Override
			public void keyTyped(KeyEvent e) {

			}

		});

		addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				play = true;

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				play = false;
				shoot = false;

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				shoot = true;

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				shoot = false;

			}

		});

	}

	public void paintComponent(Graphics g) {
		/* Init */
		Graphics2D g2 = (Graphics2D) g;
		m = new GraphicContainer(g2);
		if (!gameOver) {
			if (play) {
				/*-----Ship Moove-----*/
				mooveShip();

				int[] mouse = mousePosition();
				int[] shipD = new int[2];

				shipD[0] = shipData.getX() + shipSize / 2;
				shipD[1] = shipData.getY() + shipSize / 2;

				double angle = 0;

				angle = getAngle(shipD, mouse);

				shipData.setAngle(angle);

				/*-----Ship shoot-----*/
				if (shoot) {
					counterShot++;
					if (counterShot >= 10) {
						counterShot = 0;
						addShotPlayer(angle);
					}
				}
				calcShotPlayer();

				/*-----Hit-----*/
				hitEnemyS();
				hitEnemyA();
				hitEnemyX();

				hitShip();

				/*
				 * -----Draw-----------*
				 * 
				 * -----Background-----*
				 */
				if (this.getWidth() != framePresize[0] || this.getHeight() != framePresize[1]) {

					framePresize[0] = this.getWidth();
					framePresize[1] = this.getHeight();

					ship = ship.getScaledInstance((int) (shipSize * 100 * ((this.getWidth() / 10) / (778 / 10))) / 100,
							(int) (shipSize * 100 * ((this.getWidth() / 2) / (778 / 2))) / 100,
							BufferedImage.SCALE_SMOOTH);

					shipData.setX((int) ((getWidth() - ship.getWidth(null)) / 2));
					shipData.setY((int) ((getHeight() - 66) / 2));

					background = background.getScaledInstance(this.getWidth(), this.getHeight(),
							BufferedImage.SCALE_SMOOTH);
				}

				g2.drawImage(background, 0, 0, this);

				/*-----EnemyA-----*/
				enemyA = enemyAimMethods.carryAimEnemy(enemyA, enemyA_size, enemyA_boddy, enemyA_canon, g2, shipData,
						this.getWidth(), this.getHeight());

				/*-----EnemyS-----*/
				enemyS = enemySMethods.carrySEnemy(enemyS, enemyS_size, enemyS_img, g2, shipData, this.getWidth(),
						this.getHeight());
				enemyX = enemyXMethods.carryEnemyX(enemyX, enemyX_size, enemyX_img, g2, shipData, this.getWidth(),
						this.getHeight());

				/*-----HP-----*/
				drawHP(g2);

				/*-----Ship-----*/
				drawShotPlayer();
				if (start) {
					start = false;
					shipData.setX((int) ((getWidth() - ship.getWidth(null)) / 2) + 20);
					shipData.setY((int) ((getHeight() - 66) / 2) + 20);
				}
				AffineTransform at = new AffineTransform();
				at.setToRotation(angle, shipData.getX() + (ship.getWidth(null) / 2),
						shipData.getY() + (ship.getHeight(null) / 2));
				at.translate(shipData.getX(), shipData.getY());
				g2.setTransform(at);

				if (hit) {
					prehit = !prehit;
					counterHit++;

					if (!prehit) {
						g2.drawImage(ship, 0, 0, this);
					}
					if (counterHit >= 100) {
						hit = false;
						counterHit = 0;
					} else {

					}
				} else {
					g2.drawImage(ship, 0, 0, this);
				}

				at.setToRotation(0, shipData.getX() + (ship.getWidth(null) / 2),
						shipData.getY() + (ship.getHeight(null) / 2));
				g2.setTransform(at);

				updateScore();
			}
			if (showHitbox) {
				drawHitbox(g2);
			}

		} else {

			if (counterGO == 100) {
				counterGO++;
				JOptionPane.showMessageDialog(this, "You got " + score + " points.", "Score",
						JOptionPane.PLAIN_MESSAGE);
				System.exit(0);
			} else {
				counterGO++;
			}
			try {
				BufferedImage gover = ImageIO.read(new File("./resource/gameover.png"));
				g2.drawImage(gover, 0, 0, this);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	private void drawHP(Graphics2D g2) {
		if (shipHP >= 100) {
			shipHP = 100;
		}
		int x = 20;
		int y = 20;
		int px = shipHP * 5;
		g2.setColor(Color.white);
		g2.drawRect(x, y, 500, 10);
		g2.setColor(Color.red);
		g2.fillRect(x + 1, y + 1, px, 10);

	}

	/* Shot Player */
	private void addShotPlayer(double angle) {

		shots.add(new Shoot(shipData.getX() + shipSize / 2 + 20, shipData.getY() + shipSize / 2 + 20,
				shipData.getAngle() - Math.PI / 2));
	}

	private void calcShotPlayer() {
		int r = 5;

		int x;
		int y;
		for (int i = 0; i < shots.size(); i++) {
			x = (int) (shots.get(i).getStartX() + shots.get(i).getTimes() * r * Math.cos(shots.get(i).getAngle()));
			y = (int) (shots.get(i).getStartY() + shots.get(i).getTimes() * r * Math.sin(shots.get(i).getAngle()));

			shots.get(i).addTimes();
			if (shots.get(i).getX() < 0 || shots.get(i).getY() < 0 || shots.get(i).getX() > this.getWidth()
					|| shots.get(i).getY() > this.getHeight()) {
				shots.remove(i);
			} else {
				shots.get(i).setX((int) (x));
				shots.get(i).setY((int) (y));

			}
		}

	}

	private void drawShotPlayer() {
		int x;
		int y;
		for (int i = 0; i < shots.size(); i++) {
			x = shots.get(i).getX();
			y = shots.get(i).getY();
			m.drawBall(x, y, 3, new Color(56, 245, 245));
		}
	}

	/* Enemy */
	private void hitEnemyS() {

		for (int i = 0; i < enemyS.size(); i++) {
			for (int j = 0; j < shots.size(); j++) {
				if (shots.get(j).getX() <= enemyS.get(i).getX() + enemySize * 2 - 5
						&& shots.get(j).getX() >= enemyS.get(i).getX() - enemySize * 2 - 5) {

					if (shots.get(j).getY() <= enemyS.get(i).getY() + enemySize * 2 - 5
							&& shots.get(j).getY() >= enemyS.get(i).getY() - enemySize * 2) {

						enemyS.get(i).setLive(enemyS.get(i).getLive() + 1);
						shots.remove(j);
					}
				}
			}
			if (enemyS.get(i).getLive() >= 5) {
				enemyS.remove(i);
				shipHP++;
				score += 100;
			}
		}

	}

	private void hitEnemyA() {

		for (int i = 0; i < enemyA.size(); i++) {
			for (int j = 0; j < shots.size(); j++) {
				if ((shots.get(j).getX() <= enemyA.get(i).getX() + enemyA_size / 2 - 5)
						&& (shots.get(j).getX() >= enemyA.get(i).getX() - enemyA_size / 2 - 5)) {

					if ((shots.get(j).getY() <= enemyA.get(i).getY() + enemyA_size / 2 - 5)
							&& (shots.get(j).getY() >= enemyA.get(i).getY() - enemyA_size / 2 - 5)) {

						enemyA.get(i).setLive(enemyA.get(i).getLive() + 1);
						shots.remove(j);
					}
				}
			}
			if (enemyA.get(i).getLive() >= 10) {
				enemyA.remove(i);
				shipHP++;
				score += 100;
			}
		}

	}

	private void hitEnemyX() {

		for (int i = 0; i < enemyX.size(); i++) {
			for (int j = 0; j < shots.size(); j++) {
				if ((shots.get(j).getX() <= enemyX.get(i).getX() + enemyX_size / 4 - 5)
						&& (shots.get(j).getX() >= enemyX.get(i).getX() - enemyX_size / 4 - 5)) {

					if ((shots.get(j).getY() <= enemyX.get(i).getY() + enemyX_size / 4 - 5)
							&& (shots.get(j).getY() >= enemyX.get(i).getY() - enemyX_size / 4 - 5)) {

						enemyX.get(i).setLive(enemyX.get(i).getLive() + 1);
						shots.remove(j);
					}
				}
			}
			if (enemyX.get(i).getLive() >= 10) {
				enemyX.remove(i);
				shipHP++;
				score += 100;
			}
		}

	}

	/* Stuff */
	private int[] mousePosition() {

		int xf[] = new int[2];
		xf[0] = 0;
		xf[1] = 0;

		xf[0] = (int) StartShooter.frame.getLocation().getX();
		xf[1] = (int) StartShooter.frame.getLocation().getY();

		int xm[] = new int[2];
		xm[0] = 0;
		xm[1] = 0;

		xm[0] = (int) MouseInfo.getPointerInfo().getLocation().getX() - xf[0] - 3;
		xm[1] = (int) MouseInfo.getPointerInfo().getLocation().getY() - xf[1] - 27;

		// System.out.println(xm[0] + ", " + xm[1]);

		return xm;
	}

	private double getAngle(int[] p1, int[] p2) {
		double a = Math.PI / 2;

		double x = p2[0] - p1[0];
		double y = p1[1] - p2[1];
		double z = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));

		if (x == 0) {
			if (y > 0) {
				a = Math.PI / -2;
			} else if (y < 0) {
				a = Math.PI / 2;
			}
		} else if (y == 0) {
			if (x > 0) {
				a = 0;
			} else if (x < 0) {
				a = Math.PI;
			}
		} else if (x > 0 && y > 0) {
			a = Math.acos(x / z) * -1;
		} else if (x < 0 && y > 0) {
			a = Math.acos(x / z) * -1;
		} else if (x < 0 && y < 0) {
			a = Math.acos(x / z);
		} else if (x > 0 && y < 0) {
			a = Math.asin(y / z) * -1;
		}
		// System.out.println(x + ", " + y + ", " + z);
		// System.out.println(a);

		return a + Math.PI / 2;

	}

	private void updateScore() {
		lblScore.setText("" + score);
	}

	/* Ship */
	private void mooveShip() {
		int speed = 3;
		speed = (int) (speed * 100 * ((this.getWidth() / 2) / (778 / 2))) / 100;

		// System.out.println(shipData.getX() + ", " + shipData.getY());
		// System.out.println(moove);

		if (mooveUP) {
			shipData.setY(shipData.getY() - speed);
		}
		if (mooveLEFT) {
			shipData.setX(shipData.getX() - speed);
		}
		if (mooveDOWN) {
			shipData.setY(shipData.getY() + speed);
		}
		if (mooveRIGHT) {
			shipData.setX(shipData.getX() + speed);
		}

		if (shipData.getX() < 8) {
			shipData.setX(shipData.getX() + speed);
		} else if (shipData.getX() > this.getWidth() - shipSize - 10) {
			shipData.setX(shipData.getX() - speed);
		}

		if (shipData.getY() < 5) {
			shipData.setY(shipData.getY() + speed);
		} else if (shipData.getY() > this.getHeight() - shipSize - 10) {
			shipData.setY(shipData.getY() - speed);
		}

	}

	private void hitShip() {
		if (!hit) {
			/* enemyS */
			for (int i = 0; i < enemyS.size(); i++) {
				if (enemyS.get(i).getX() + enemySize > shipData.getX()
						&& enemyS.get(i).getX() - enemySize < shipData.getX() + shipSize
						&& enemyS.get(i).getY() + enemySize > shipData.getY()
						&& enemyS.get(i).getY() - enemySize < shipData.getY() + shipSize) {

					shipHP -= 10;
					score -= 25;
					enemyS.remove(i);
					hit = true;
				} else {
					for (int j = 0; j < enemyS.get(i).getShots().size(); j++) {
						if (enemyS.get(i).getShot(j).getX() + shipSize > shipData.getX()
								&& enemyS.get(i).getShot(j).getX() - shipSize < shipData.getX() + shipSize
								&& enemyS.get(i).getShot(j).getY() + shipSize > shipData.getY()
								&& enemyS.get(i).getShot(j).getY() - shipSize < shipData.getY() + shipSize) {
							shipHP -= 5;
							score -= 10;
							enemyS.get(i).removeShot(j);
							hit = true;
						}
					}
				}

			}
			/* EnemyA */
			for (int i = 0; i < enemyA.size(); i++) {

				if (enemyA.get(i).getX() + enemyA_size > shipData.getX()
						&& enemyA.get(i).getX() - enemyA_size < shipData.getX() + shipSize
						&& enemyA.get(i).getY() + enemyA_size > shipData.getY()
						&& enemyA.get(i).getY() - enemyA_size < shipData.getY() + shipSize) {

					shipHP -= 10;
					score -= 25;
					enemyA.remove(i);
					hit = true;
				} else {
					for (int j = 0; j < enemyA.get(i).getShotSize(); j++) {
						if (enemyA.get(i).getShot(j).getX() + shipSize > shipData.getX()
								&& enemyA.get(i).getShot(j).getX() - shipSize < shipData.getX() + shipSize
								&& enemyA.get(i).getShot(j).getY() + shipSize > shipData.getY()
								&& enemyA.get(i).getShot(j).getY() - shipSize < shipData.getY() + shipSize) {
							shipHP -= 5;
							score -= 10;
							enemyA.get(i).removeShot(j);
							hit = true;
						}
					}
				}

			} /* EnemyX */
			for (int i = 0; i < enemyX.size(); i++) {

				if (enemyX.get(i).getX() + enemyX_size > shipData.getX()
						&& enemyX.get(i).getX() - enemyX_size < shipData.getX() + shipSize
						&& enemyX.get(i).getY() + enemyX_size > shipData.getY()
						&& enemyX.get(i).getY() - enemyX_size < shipData.getY() + shipSize) {

					shipHP -= 10;
					score -= 25;
					enemyX.remove(i);
					hit = true;
				} else {
					for (int j = 0; j < enemyX.get(i).getShotSize(); j++) {
						if (enemyX.get(i).getShot(j).getX()+shipSize > shipData.getX()
								&& enemyX.get(i).getShot(j).getX() - shipSize < shipData.getX() + shipSize
								&& enemyX.get(i).getShot(j).getY() + shipSize > shipData.getY()
								&& enemyX.get(i).getShot(j).getY() - shipSize < shipData.getY() + shipSize) {
							shipHP -= 5;
							score -= 10;
							enemyX.get(i).removeShot(j);
							hit = true;
						}
					}
				}

			}
			if (shipHP <= 0) {
				gameOver = true;
			}
		}
	}

	private void drawHitbox(Graphics2D g2) {
		g2.setColor(Color.red);
		g2.drawRect(shipData.getX(), shipData.getY(), 100, 100);
	}

	public JLabel getLblAutor() {
		if (lblAutor == null) {
			lblAutor = new JLabel("\u00a9 Michael Lintschinger");
			lblAutor.setForeground(Color.WHITE);
			lblAutor.setBounds(1600 - 150, 900 - 57, 141, 16);
		}
		return lblAutor;
	}

	public JLabel getLabel() {
		if (label == null) {
			label = new JLabel("\u00A9 Michael Lintschinger");
			label.setBounds(1600 - 152, 900 - 55, 141, 16);
		}
		return label;
	}
}
