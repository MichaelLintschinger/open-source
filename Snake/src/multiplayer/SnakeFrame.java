package multiplayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.Timer;

import multiplayer.RenderPanel;

@SuppressWarnings("serial")
public class SnakeFrame extends JFrame implements ActionListener {

	Timer timer = new Timer(1, this);
	RenderPanel renderpanel;

	SnakeFrame(int lvldif, int player) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Snake_Multiplayer");
		setSize(800, 700);
		setLocationRelativeTo(null);
		add(renderpanel = new RenderPanel(player, lvldif));
		setVisible(true);
		timer.start();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		renderpanel.repaint();
	}

}
