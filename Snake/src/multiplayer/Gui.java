package multiplayer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Choice;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.UIManager;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;

@SuppressWarnings("serial")
public class Gui extends JFrame {
	JTabbedPane tabbedPane;
	JPanel tab1;
	JPanel tab2;
	Choice difficultyLVL;
	Choice amountPlayer;
	int lvldif = 1;
	int player = 1;
	private JLabel lblPlayer1;
	private JLabel lblPlayer2;
	private JLabel lblPlayer3;
	private JLabel lblPlayer4;
	private JLabel lblUp1;
	private JLabel lblUp3;
	private JLabel lblUp2;
	private JLabel lblUp4;
	private JLabel lblDown1;
	private JLabel lblDown3;
	private JLabel lblDown4;
	private JLabel lblDown2;
	private JLabel lblLeft1;
	private JLabel lblRight1;
	private JLabel lblLeft3;
	private JLabel lblLeft4;
	private JLabel lblLeft2;
	private JLabel lblRight3;
	private JLabel lblRight4;
	private JLabel lblRight2;
	private JLabel down1;
	private JLabel left1;
	private JLabel right1;
	private JLabel down2;
	private JLabel left2;
	private JLabel right2;
	private JLabel down3;
	private JLabel left3;
	private JLabel right3;
	private JLabel down4;
	private JLabel left4;
	private JLabel right4;

	Gui() {
		setResizable(false);
		setTitle("Snake_Multiplayer");
		setSize(500, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		getContentPane().setLayout(null);

		// tabbedPane
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 500, 370);
		getContentPane().add(tabbedPane);

		tab1 = new JPanel();
		tabbedPane.addTab("Start", null, tab1, null);
		tab1.setLayout(null);

		// Choice
		difficultyLVL = new Choice();
		difficultyLVL.setSize(186, 22);
		difficultyLVL.setLocation(10, 43);
		difficultyLVL.setBackground(UIManager.getColor("Button.background"));
		difficultyLVL.add("---select DifficultyLVL---");
		difficultyLVL.add("Easy");
		difficultyLVL.add("Middle");
		difficultyLVL.add("Hard");
		difficultyLVL.add("Extreme");
		tab1.add(difficultyLVL);

		amountPlayer = new Choice();
		amountPlayer.setBackground(UIManager.getColor("Button.background"));
		amountPlayer.add("---select the amount of player---");
		amountPlayer.add("1");
		amountPlayer.add("2");
		amountPlayer.add("3");
		amountPlayer.add("4");
		amountPlayer.setBounds(260, 43, 225, 20);
		tab1.add(amountPlayer);

		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(isAllSet()){
					setAmountPlayer();
					setLVLDiff();
					startSnake();
				}
			}
		});
		btnStart.setBounds(190, 156, 120, 40);
		tab1.add(btnStart);

		JLabel lblDifficultyLevel = new JLabel("Difficulty Level");
		lblDifficultyLevel.setBounds(10, 23, 88, 14);
		tab1.add(lblDifficultyLevel);

		JLabel lblAmountOfPlayer = new JLabel("Amount of Player");
		lblAmountOfPlayer.setBounds(260, 23, 120, 14);
		tab1.add(lblAmountOfPlayer);

		tab2 = new JPanel();
		tabbedPane.addTab("Controlls", null, tab2, null);
		tab2.setLayout(null);
		
		lblPlayer1 = new JLabel("Player1");
		lblPlayer1.setBounds(10, 11, 46, 14);
		tab2.add(lblPlayer1);
		
		lblPlayer2 = new JLabel("Player 2");
		lblPlayer2.setBounds(276, 11, 46, 14);
		tab2.add(lblPlayer2);
		
		lblPlayer3 = new JLabel("Player 3");
		lblPlayer3.setBounds(10, 166, 46, 14);
		tab2.add(lblPlayer3);
		
		lblPlayer4 = new JLabel("Player 4");
		lblPlayer4.setBounds(276, 166, 46, 14);
		tab2.add(lblPlayer4);
		
		lblUp1 = new JLabel("up:");
		lblUp1.setBounds(10, 43, 46, 14);
		tab2.add(lblUp1);
		
		lblUp2 = new JLabel("up:");
		lblUp2.setBounds(276, 43, 46, 14);
		tab2.add(lblUp2);
		
		lblUp3 = new JLabel("up:");
		lblUp3.setBounds(10, 198, 46, 14);
		tab2.add(lblUp3);
		
		lblUp4 = new JLabel("up:");
		lblUp4.setBounds(276, 198, 46, 14);
		tab2.add(lblUp4);
		
		lblDown1 = new JLabel("down:");
		lblDown1.setBounds(10, 68, 46, 14);
		tab2.add(lblDown1);
		
		lblDown2 = new JLabel("down:");
		lblDown2.setBounds(276, 68, 46, 14);
		tab2.add(lblDown2);
		
		lblDown3 = new JLabel("down:");
		lblDown3.setBounds(10, 223, 46, 14);
		tab2.add(lblDown3);
		
		lblDown4 = new JLabel("down:");
		lblDown4.setBounds(276, 223, 46, 14);
		tab2.add(lblDown4);
		
		lblLeft1 = new JLabel("left:");
		lblLeft1.setBounds(10, 93, 46, 14);
		tab2.add(lblLeft1);
		
		lblLeft3 = new JLabel("left:");
		lblLeft3.setBounds(10, 248, 46, 14);
		tab2.add(lblLeft3);
		
		lblLeft2 = new JLabel("left:");
		lblLeft2.setBounds(276, 93, 46, 14);
		tab2.add(lblLeft2);
		
		lblLeft4 = new JLabel("left:");
		lblLeft4.setBounds(276, 248, 46, 14);
		tab2.add(lblLeft4);
		
		lblRight1 = new JLabel("right:");
		lblRight1.setBounds(10, 117, 46, 14);
		tab2.add(lblRight1);
		
		lblRight2 = new JLabel("right:");
		lblRight2.setBounds(276, 117, 46, 14);
		tab2.add(lblRight2);
		
		lblRight3 = new JLabel("right:");
		lblRight3.setBounds(10, 273, 46, 14);
		tab2.add(lblRight3);
		
		lblRight4 = new JLabel("right:");
		lblRight4.setBounds(276, 273, 46, 14);
		tab2.add(lblRight4);
		
		JLabel up1 = new JLabel("w");
		up1.setBounds(80, 43, 46, 14);
		tab2.add(up1);
		
		down1 = new JLabel("s");
		down1.setBounds(80, 68, 46, 14);
		tab2.add(down1);
		
		left1 = new JLabel("a");
		left1.setBounds(80, 93, 46, 14);
		tab2.add(left1);
		
		right1 = new JLabel("d");
		right1.setBounds(80, 117, 46, 14);
		tab2.add(right1);
		
		JLabel up2 = new JLabel("i");
		up2.setBounds(340, 43, 46, 14);
		tab2.add(up2);
		
		down2 = new JLabel("k");
		down2.setBounds(340, 68, 24, 14);
		tab2.add(down2);
		
		left2 = new JLabel("j");
		left2.setBounds(340, 93, 46, 14);
		tab2.add(left2);
		
		right2 = new JLabel("l");
		right2.setBounds(340, 117, 46, 14);
		tab2.add(right2);
		
		JLabel up3 = new JLabel("up");
		up3.setBounds(80, 198, 46, 14);
		tab2.add(up3);
		
		down3 = new JLabel("down");
		down3.setBounds(80, 223, 46, 14);
		tab2.add(down3);
		
		left3 = new JLabel("left");
		left3.setBounds(80, 248, 46, 14);
		tab2.add(left3);
		
		right3 = new JLabel("right");
		right3.setBounds(80, 273, 46, 14);
		tab2.add(right3);
		
		JLabel up4 = new JLabel("5");
		up4.setBounds(340, 198, 46, 14);
		tab2.add(up4);
		
		down4 = new JLabel("2");
		down4.setBounds(340, 223, 46, 14);
		tab2.add(down4);
		
		left4 = new JLabel("1");
		left4.setBounds(340, 248, 46, 14);
		tab2.add(left4);
		
		right4 = new JLabel("3");
		right4.setBounds(340, 273, 46, 14);
		tab2.add(right4);
		setVisible(true);

	}

	private void startSnake() {
		this.setVisible(false);
		new SnakeFrame(lvldif, player);
	}

	private boolean isAllSet() {
		boolean tr = true;
		if (amountPlayer.getSelectedIndex() == 0) {
			System.out.println("please select an amount of player.");
			tr = false;
		}
		if (difficultyLVL.getSelectedIndex() == 0) {
			System.out.println("please select a difficultyLVL.");
			tr = false;
		}
		return tr;
	}

	private void setAmountPlayer() {
		player = amountPlayer.getSelectedIndex();

	}

	private void setLVLDiff() {
		lvldif = difficultyLVL.getSelectedIndex();
	}

	public static void main(String[] args) {
		new Gui();
	}
}
