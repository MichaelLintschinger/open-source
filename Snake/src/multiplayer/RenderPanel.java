package multiplayer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Font;

@SuppressWarnings("serial")
public class RenderPanel extends JPanel {
	// 770
	// 650
	ArrayList<Integer> parts[][];
	Graphics2D g;
	boolean start;
	boolean playing[];
	boolean gameover = false;

	boolean drawDot = true;
	JLabel gameOver;
	int winner;
	int amountOfPlayer;
	int lvldiff;
	int speed;
	int counter;
	int counterspeed = 1;
	int xDot;
	int yDot;
	int xHead[];
	int yHead[];
	int xDel[];
	int yDel[];
	int predirec[];
	int direc[];
	int direcd2[];
	int length[];
	String playerName[];

	@SuppressWarnings("unchecked")
	RenderPanel(int amountOfPlayer, int lvldiff) {
		winner = 0;
		speed = 200;
		counter = 0;
		setForeground(Color.black);
		setBackground(Color.black);
		start = true;
		this.amountOfPlayer = amountOfPlayer;
		this.lvldiff = lvldiff*2;
		parts = new ArrayList[amountOfPlayer][2];
		length = new int[amountOfPlayer];
		playing = new boolean[amountOfPlayer];

		for (int i = 0; i < amountOfPlayer; i++) {
			length[i] = 2;
			playing[i] = true;
			parts[i][0] = new ArrayList<Integer>(length[i]);
			parts[i][1] = new ArrayList<Integer>(length[i]);
			for (int j = 0; j < length[i]; j++) {
				parts[i][0].add(1);
				parts[i][1].add(1);
			}
		}
		setFocusable(true);
		addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {
				setDir(e);
			}

			@Override
			public void keyTyped(KeyEvent e) {

			}
		});

		gameOver = new JLabel("GameOver");
		gameOver.setVisible(false);
		gameOver.setSize(658, 57);
		gameOver.setLocation(131, 295);
		gameOver.setFont(new Font("Tahoma", Font.PLAIN, 60));
		gameOver.setForeground(Color.GRAY);
		add(gameOver);
		createSnakeArmy();
		setFocusable(true);
		setLayout(null);
	}

	public void paintComponent(Graphics g2) {
		counter++;
		g = (Graphics2D) g2;

		if (counter % (int) (speed / lvldiff) == 0) {
			if (gameover == false) {
				if (start) {
					System.out.println("start");
					clear();
					start = false;

				} else {
					redraw();
					for (int i = 0; i < amountOfPlayer; i++) {
						predirec[i] = direc[i];
					}
				}
			} else {
				deathScreen();
			}
		}
	}

	private void deathScreen() {
		clear();
		gameOver.setText("Winner: Player " + winner);
		gameOver.setVisible(true);
	}

	private void clear() {
		g.setBackground(Color.BLACK);
		g.clearRect(0, 0, 800, 800);

	}

	private void redraw() {
		int counter = 0;
		proofGaming();
		gameover = true;
		for (int i = 0; i < amountOfPlayer; i++) {
			if (playing[i]) {
				counter++;
				winner = i + 1;
				gameover = false;
				drawKillPoint(i);
				drawHead(i);
				drawPart(i);
			} else {
				drawDead(i);
			}
		}
		if (counter == 1 && amountOfPlayer > 1) {
			gameover = true;
		}
		whitePoint();
		for (int i = 0; i < amountOfPlayer; i++) {
			if (xDot == xHead[i] && yDot == yHead[i]) {
				length[i]++;
				parts[i][0].add(0, 0);
				parts[i][1].add(0, 0);
				drawDot = true;
			}
		}

	}

	private void drawDead(int i) {
		for (int j = 0; j < length[i]; j++) {
			drawPoint(parts[i][0].get(j), parts[i][1].get(j), Color.gray, 16);
		}
	}

	private void proofGaming() {
		for (int i = 0; i < amountOfPlayer; i++) {
			for (int j = 0; j < amountOfPlayer; j++) {
				if (i != j) {
					if (parts[j][0].contains(xHead[i]) && parts[j][1].contains(yHead[i])) {
						playing[i] = false;
						System.out.println(parts[i][0].toString());
						System.out.println(parts[i][1].toString());
					}
				}
			}
			if (playing[i]) {
				if (xHead[i] < 10 || xHead[i] > 770 || yHead[i] < 10 || yHead[i] > 650) {
					playing[i] = false;
				}
			}
		}
	}

	private void drawKillPoint(int i) {

		parts[i][0].set(length[i] - 1, xHead[i]);
		parts[i][1].set(length[i] - 1, yHead[i]);
		xDel[i] = parts[i][0].get(0);
		yDel[i] = parts[i][1].get(0);
		for (int j = 0; j < length[i] - 1; j++) {
			parts[i][0].set(j, parts[i][0].get(j + 1));
			parts[i][1].set(j, parts[i][1].get(j + 1));
		}
		drawPoint(xDel[i], yDel[i], Color.black, 20);
	}

	private void drawHead(int i) {
		calcPosition(i);
		if (i == 0) {
			g.setColor(Color.green);
		} else if (i == 1) {
			g.setColor(Color.red);
		} else if (i == 2) {
			g.setColor(Color.blue);
		} else if (i == 3) {
			g.setColor(Color.yellow);
		}
		drawPoint(xHead[i], yHead[i], g.getColor(), 20);
	}

	private void drawPart(int i) {

		drawPoint(parts[i][0].get(length[i] - 1), parts[i][1].get(length[i] - 1), Color.black, 20);
		if (i == 0) {
			drawPoint(parts[i][0].get(length[i] - 1), parts[i][1].get(length[i] - 1), Color.green, 16);
		} else if (i == 1) {
			drawPoint(parts[i][0].get(length[i] - 1), parts[i][1].get(length[i] - 1), Color.red, 16);
		} else if (i == 2) {
			drawPoint(parts[i][0].get(length[i] - 1), parts[i][1].get(length[i] - 1), Color.blue, 16);
		} else if (i == 3) {
			drawPoint(parts[i][0].get(length[i] - 1), parts[i][1].get(length[i] - 1), Color.yellow, 16);
		}
	}

	private void createSnakeArmy() {
		xHead = new int[amountOfPlayer];
		yHead = new int[amountOfPlayer];
		xDel = new int[amountOfPlayer];
		yDel = new int[amountOfPlayer];
		direc = new int[amountOfPlayer];
		predirec = new int[amountOfPlayer];

		for (int i = 0; i < amountOfPlayer; i++) {
			xHead[i] = 10;
			if (i == 0) {
				yHead[i] = 90;
				direc[i] = 68;
			} else if (i == 1) {
				yHead[i] = 250;
				direc[i] = 76;
			} else if (i == 2) {
				yHead[i] = 410;
				direc[i] = 39;
			} else if (i == 3) {
				yHead[i] = 570;
				direc[i] = 99;
			}
		}
	}

	private void calcPosition(int i) {
		if (direc[i] == 87 || direc[i] == 38 || direc[i] == 73 || direc[i] == 101) {
			yHead[i] -= 20;
		} else if (direc[i] == 65 || direc[i] == 37 || direc[i] == 74 || direc[i] == 97) {
			xHead[i] -= 20;
		} else if (direc[i] == 83 || direc[i] == 40 || direc[i] == 75 || direc[i] == 98) {
			yHead[i] += 20;
		} else if (direc[i] == 68 || direc[i] == 39 || direc[i] == 76 || direc[i] == 99) {
			xHead[i] += 20;
		}
	}

	private void setDir(KeyEvent e) {
		if (e.getKeyCode() == 65 || e.getKeyCode() == 83 || e.getKeyCode() == 68 || e.getKeyCode() == 87) {
			if (amountOfPlayer <= 0) {
			} else {
				if (proofdirec(e.getKeyCode(), predirec[0], 0)) {
					direc[0] = e.getKeyCode();
				}
			}
		} else if (e.getKeyCode() == 73 || e.getKeyCode() == 74 || e.getKeyCode() == 75 || e.getKeyCode() == 76) {
			if (amountOfPlayer <= 1) {
			} else {
				if (proofdirec(e.getKeyCode(), predirec[1], 1)) {
					direc[1] = e.getKeyCode();
				}
			}
		} else if (e.getKeyCode() == 37 || e.getKeyCode() == 38 || e.getKeyCode() == 39 || e.getKeyCode() == 40) {
			if (amountOfPlayer <= 2) {
			} else {
				if (proofdirec(e.getKeyCode(), predirec[2], 2)) {
					direc[2] = e.getKeyCode();
				}
			}
		} else if (e.getKeyCode() == 97 || e.getKeyCode() == 98 || e.getKeyCode() == 99 || e.getKeyCode() == 101) {
			if (amountOfPlayer <= 3) {
			} else {
				if (proofdirec(e.getKeyCode(), predirec[3], 3)) {
					direc[3] = e.getKeyCode();
				}
			}

		}
	}

	private boolean proofdirec(int i, int j, int index) {
		boolean tr = true;
		// System.out.println("Index=" + index + " i=" + i + " j=" + j);
		if (index == 0) {
			if (i == 87 && j == 83 || i == 65 && j == 68 || i == 83 && j == 87 || i == 68 && j == 65) {
				tr = false;
			}
		} else if (index == 1) {
			if (i == 73 && j == 75 || i == 74 && j == 76 || i == 75 && j == 73 || i == 76 && j == 74) {
				tr = false;
			}
		} else if (index == 2) {
			if (i == 38 && j == 40 || i == 37 && j == 39 || i == 40 && j == 38 || i == 39 && j == 37) {
				tr = false;
			}
		} else if (index == 3) {
			if (i == 101 && j == 98 || i == 97 && j == 99 || i == 98 && j == 101 || i == 99 && j == 97) {
				tr = false;
			}
		}
		return tr;
	}

	private void whitePoint() {
		if (drawDot || start) {
			drawDot = false;
			for (int i = 0; i < 1;) {
				xDot = ThreadLocalRandom.current().nextInt(10, 770);
				if ((xDot - 10) % 20 == 0) {
					i++;
				}
			}
			for (int i = 0; i < 1;) {
				yDot = ThreadLocalRandom.current().nextInt(50, 650);
				if ((yDot - 10) % 20 == 0) {
					i++;
				}
			}
			counterspeed++;
			if (counterspeed == 10) {
				lvldiff++;
				counterspeed = 0;
			}
		}
		drawPoint(xDot, yDot, Color.white, 10);
	}

	public void drawPoint(int x, int y) {
		g.setColor(Color.white);
		g.drawLine(x, y, x, y);
	}

	public void drawPoint(int x, int y, Color color) {
		g.setColor(color);
		g.drawLine(x, y, x, y);
	}

	public void drawPoint(int x, int y, int st�rke) {
		g.setColor(Color.white);
		x += st�rke / 2;
		y += st�rke / 2;
		for (int i = 0; i < st�rke; i++) {
			g.drawLine(x + i, y, x + i, y + st�rke - 1);
		}
	}

	public void drawPoint(int x, int y, Color color, int st�rke) {
		g.setColor(color);
		x -= st�rke / 2;
		y -= st�rke / 2;
		for (int i = 0; i < st�rke; i++) {
			g.drawLine(x + i, y, x + i, y + st�rke - 1);
		}
	}
}
