package singleplayer;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class RenderPanel extends JPanel {

	/*
	 * Border: x: 9-771; y: 9-651;
	 */
	public int diflvl;
	public int countertime = 0;
	public int counterspeed = 0;
	public boolean drawDot = true;
	public Snake snake = new Snake();
	public Graphics2D g;
	public char direc1 = 'd';
	public char direcprev1 = 'd';
	public int snakeParts1 = 1 + 1;
	public int x1;
	public int y1;
	public int xDot;
	public int yDot;
	public int xBlack1;
	public int yBlack1;
	public boolean start = true;
	public double speed = 1;
	public boolean gaming = true;
	private boolean pause = false;
	public JLabel gameOver;
	JLabel lbllvldif;

	@SuppressWarnings("unchecked")
	ArrayList<Integer> partXY[] = new ArrayList[2];
	private JTextField txtPoints;
	private JTextField txtSpeed;

	RenderPanel(double speed, int lvldif) {
		diflvl = lvldif;
		this.speed = speed;
		System.out.println("finish");
		partXY[0] = new ArrayList<Integer>(1);
		partXY[1] = new ArrayList<Integer>(1);
		for (int i = 0; i < snakeParts1; i++) {
			partXY[0].add(1 + i);
			partXY[1].add(1 + i);
		}
		setLayout(null);
		setFocusable(true);

		txtPoints = new JTextField();
		txtPoints.setEditable(false);
		txtPoints.setText("Points: 1");
		txtPoints.setBounds(0, 0, 124, 22);
		txtPoints.setColumns(10);
		add(txtPoints);

		txtSpeed = new JTextField();
		txtSpeed.setEditable(false);
		txtSpeed.setText("Speed: ");
		txtSpeed.setBounds(684, 0, 116, 22);
		add(txtSpeed);
		txtSpeed.setColumns(10);

		lbllvldif = new JLabel("");
		if (diflvl == 1) {
			lbllvldif.setText("Easy");
		} else if (diflvl == 2) {
			lbllvldif.setText("Middle");
		} else if (diflvl == 3) {
			lbllvldif.setText("Hard");
		} else if (diflvl == 4) {
			lbllvldif.setText("Extreme");
		}
		lbllvldif.setBounds(357, 3, 56, 16);
		add(lbllvldif);

		addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent e) {

			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyChar() == ' ') {
					if (pause == false) {
						pause = true;
					} else if (pause) {
						pause = false;
					}
				}
			}

			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyChar() == 'w') {
					if (direcprev1 == 's') {

					} else {
						direc1 = e.getKeyChar();
					}

				}
				if (e.getKeyChar() == 'a') {
					if (direcprev1 == 'd') {

					} else {

						direc1 = e.getKeyChar();
					}
				}
				if (e.getKeyChar() == 's') {
					if (direcprev1 == 'w') {

					} else {
						direc1 = e.getKeyChar();
					}
				}
				if (e.getKeyChar() == 'd') {
					if (direcprev1 == 'a') {

					} else {
						direc1 = e.getKeyChar();
					}
				}
			}
		});
	}

	public void testGameOver() {
		for (int i = 0; i < snakeParts1 - 1; i++) {
			if (x1 == partXY[0].get(i)) {
				if (y1 == partXY[1].get(i)) {
					gaming = false;
				}
			}
		}
	}

	public void paintComponent(Graphics g2) {
		txtSpeed.setText("Speed: " + speed / 20);
		countertime++;
		if (countertime == (int) (1000 / speed)) {
			countertime = 0;
			direcprev1 = direc1;
			if (gaming) {
				if (pause) {
				} else {
					g = (Graphics2D) g2;
					// Border
					g.setColor(Color.red);
					g.drawRect(0, 19, 793, 645);
					// set color
					g.setColor(Color.black);
					g.setBackground(Color.black);
					// count Milliseconds
					if (start) {
						g.fillRect(0, 0, 800, 700);
					}
					snakeMovement();
					start = false;

				}
			}
			if (gaming == false && pause == false) {
				gameOver = new JLabel("Game Over");
				gameOver.setFont(new Font("Tahoma", Font.BOLD, 32));
				gameOver.setBounds(322, 318, 188, 39);
				gameOver.setVisible(false);
				gameOver.setVisible(true);
				add(gameOver);

			}
			if (gaming == false && pause) {
				g.setColor(Color.black);
				g.fillRect(0, 0, 800, 700);
				x1 = 10;
				y1 = 50;
				pause = false;
				gaming = true;
				if (diflvl == 1) {
					speed = 20;
				} else if (diflvl == 2) {
					speed = 40;
				} else if (diflvl == 3) {
					speed = 70;
				} else if (diflvl == 4) {
					speed = 100;
				}
				snakeParts1 = 1 + 1;
				direc1 = 'd';
				repaint();
			}
		}
	}

	void snakeMovement() {
		snake.setDir(direc1);
		snake.calcXY();
		x1 = snake.getX();
		y1 = snake.getY();
		drawDot = false;
		if (x1 < 10 || y1 < 30 || x1 > 770 || y1 > 650) {
			gaming = false;
		} else {
			if (x1 == xDot && y1 == yDot) {
				drawDot = true;
				snakeParts1++;
				txtPoints.setText("Points: " + (snakeParts1 - 1));
				partXY[1].add(0, partXY[1].get(0));
				partXY[0].add(0, partXY[0].get(0));
			}
			drawDot();
			drawSnake();
			drawEyes();
			testGameOver();
		}
	}

	public void drawDot() {
		if (drawDot || start) {
			for (int i = 0; i < 1;) {
				xDot = ThreadLocalRandom.current().nextInt(10, 770);
				if ((xDot - 10) % 20 == 0) {
					i++;
				}
			}
			for (int i = 0; i < 1;) {
				yDot = ThreadLocalRandom.current().nextInt(50, 650);
				if ((yDot - 10) % 20 == 0) {
					i++;
				}
			}
			drawPoint(xDot, yDot, Color.white, 10);
			counterspeed++;
			if (counterspeed == 10) {
				speed += diflvl * 3;
				counterspeed = 0;
			}
		}

	}

	public void drawSnake() {
		partXY[0].set(snakeParts1 - 1, x1 - 9);
		partXY[1].set(snakeParts1 - 1, y1);

		dirCalculator(snakeParts1);

		// drawParts
		drawPoint(partXY[0].get(snakeParts1 - 1), partXY[1].get(snakeParts1 - 1), Color.black, 21);
		drawPoint(partXY[0].get(snakeParts1 - 1), partXY[1].get(snakeParts1 - 1), Color.green, 15);
		// clearPoint
		clearPoint();
		// Snake Head
		drawPoint(x1, y1, Color.green, 20);

	}

	public void drawEyes() {
		g.setColor(Color.white);
		if (direc1 == 'w') {
			// eye
			g.setColor(Color.white);
			g.fillRect(x1 - 7, y1 - 10, 4, 6);
			g.fillRect(x1 + 3, y1 - 10, 4, 6);
			// pupille
			g.setColor(Color.black);
			g.fillRect(x1 - 5, y1 - 10, 2, 4);
			g.fillRect(x1 + 3, y1 - 10, 2, 4);
		} else if (direc1 == 'a') {
			// eye
			g.setColor(Color.white);
			g.fillRect(x1 - 9, y1 - 6, 6, 4);
			g.fillRect(x1 - 9, y1 + 2, 6, 4);
			// pupille
			g.setColor(Color.black);
			g.fillRect(x1 - 10, y1 - 4, 4, 2);
			g.fillRect(x1 - 10, y1 + 2, 4, 2);
		} else if (direc1 == 's') {
			// eye
			g.setColor(Color.white);
			g.fillRect(x1 - 7, y1 + 4, 4, 6);
			g.fillRect(x1 + 3, y1 + 4, 4, 6);
			// pupille
			g.setColor(Color.black);
			g.fillRect(x1 - 5, y1 + 6, 2, 4);
			g.fillRect(x1 + 3, y1 + 6, 2, 4);
		} else if (direc1 == 'd') {
			// eye
			g.setColor(Color.white);
			g.fillRect(x1 + 4, y1 - 6, 6, 4);
			g.fillRect(x1 + 4, y1 + 2, 6, 4);
			// pupille
			g.setColor(Color.black);
			g.fillRect(x1 + 6, y1 - 4, 4, 2);
			g.fillRect(x1 + 6, y1 + 2, 4, 2);
		}

	}

	public void clearPoint() {
		xBlack1 = partXY[0].get(0);
		yBlack1 = partXY[1].get(0);
		for (int i = 0; i < snakeParts1 - 1; i++) {
			partXY[0].set(i, partXY[0].get(i + 1));
			partXY[1].set(i, partXY[1].get(i + 1));
		}

		drawPoint(xBlack1, yBlack1, Color.black, 22);
		drawPoint(xDot, yDot, Color.white, 10);
	}

	public void dirCalculator(int l) {

		if (direc1 == 'w') {
			partXY[0].set(l - 1, partXY[0].get(l - 1) + 9);
			partXY[1].set(l - 1, partXY[1].get(l - 1) + 20);
		}
		if (direc1 == 'a') {
			partXY[0].set(l - 1, partXY[0].get(l - 1) + 29);
		}
		if (direc1 == 's') {
			partXY[0].set(l - 1, partXY[0].get(l - 1) + 9);
			partXY[1].set(l - 1, partXY[1].get(l - 1) - 20);
		}
		if (direc1 == 'd') {
			partXY[0].set(l - 1, partXY[0].get(l - 1) - 11);
		}
	}

	public void drawPoint(int x, int y) {
		g.setColor(Color.white);
		g.drawLine(x, y, x, y);
	}

	public void drawPoint(int x, int y, Color color) {
		g.setColor(color);
		g.drawLine(x, y, x, y);
	}

	public void drawPoint(int x, int y, int st�rke) {
		g.setColor(Color.white);
		x += st�rke / 2;
		y += st�rke / 2;
		for (int i = 0; i < st�rke; i++) {
			g.drawLine(x + i, y, x + i, y + st�rke - 1);
		}
	}

	public void drawPoint(int x, int y, Color color, int st�rke) {
		g.setColor(color);
		x -= st�rke / 2;
		y -= st�rke / 2;
		for (int i = 0; i < st�rke; i++) {
			g.drawLine(x + i, y, x + i, y + st�rke - 1);
		}
	}
}
