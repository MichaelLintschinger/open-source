package singleplayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class Game extends JFrame implements ActionListener {

	public RenderPanel renderPanel;
	public Timer timer;
	public double speed = 1;
	public int diflvl;
	public boolean pause = false;

	public Game() {
		String[] choices = { "Easy", "Middle", "Hard", "Extreme" };
		String finalchoice = (String) JOptionPane.showInputDialog(null, "Choice", "Choice",
				JOptionPane.QUESTION_MESSAGE, null,

				choices, // Array of choices
				choices[0]); // Initial choice
		if (finalchoice.equals("Easy")) {
			speed = 10;
			diflvl = 1;
		} else if (finalchoice.equals("Middle")) {
			speed = 20;
			diflvl = 2;
		} else if (finalchoice.equals("Hard")) {
			speed = 35;
			diflvl = 3;
		} else if (finalchoice.equals("Extreme")) {
			speed = 60;
			diflvl = 4;
		}
		setTitle("Snake.jar");
		setSize(800, 700);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		add(renderPanel = new RenderPanel(speed, diflvl));
		setVisible(true);
		timer = new Timer(1, this);
		timer.start();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		renderPanel.repaint();
	}

	public static void main(String[] args) {
		new Game();
	}

}