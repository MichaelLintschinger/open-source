package singleplayer;

public class Snake {

	public int x = 10;
	public int y = 50;
	public char direc = 'd';

	public Snake() {

	}

	public void calcXY() {
		if (direc == 'w') {
			y -= 20;
		}
		if (direc == 'a') {
			x -= 20;
		}
		if (direc == 's') {
			y += 20;
		}
		if (direc == 'd') {
			x += 20;
		}
	}

	public int getX() {
		return x;

	}

	public int getY() {
		return y;

	}

	public void setDir(char c) {
		direc = c;

	}
}
