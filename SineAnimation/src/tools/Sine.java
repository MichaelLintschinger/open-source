package tools;

import java.awt.*;
import java.util.*;

public class Sine {
	/* Sinus=a*sin(wt); */
	int accuraty = 20;
	double f;// in Hz
	double d;// in rad
	int a;// in 100p
	double w;

	ArrayList<Point> dots;

	public Sine(double f, int a) {
		dots = new ArrayList<Point>();
		setD(0.0);
		setF(f);
		setA(a);
	}

	public void tick(double f, int a, int ac, double speed) {
		accuraty = ac;
		setF(f);
		setA(a);
		d += speed;
		if (d >= 2 * Math.PI) {
			d = 0.0;
		}
		Thread1[] th = new Thread1[(int) (2 * Math.PI * accuraty)];
		double t = 0;
		for (int i = 0; i < th.length; i++) {
			t += 1.0 / accuraty;
			try {
				dots.get(i);
			} catch (IndexOutOfBoundsException e) {
				dots.add(new Point());
			}
			th[i] = new Thread1(dots.get(i), t, w, a, d);
			th[i].start();

		}

		try {
			Thread.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		int j = 0;
		for (int i = 0; i < th.length; i++) {
			dots.set(i, th[i].getPoint());
			j = i;
		}
		j++;
		int k = 0;
		while (k == 0) {
			try {
				dots.remove(j);
			} catch (IndexOutOfBoundsException e) {
				k++;
			}
		}
	}

	public ArrayList<Point> getDots() {
		return dots;
	}

	public void setDots(ArrayList<Point> dots) {
		this.dots = dots;
	}

	/* Getter/Setter */
	public double getF() {
		return f;
	}

	public void setF(double f) {
		this.f = f;
		this.w = f;
	}

	private void setD(double d2) {
		this.d = d2;

	}

	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

}
