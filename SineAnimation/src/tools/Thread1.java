package tools;

import java.awt.Point;

public class Thread1 extends Thread {
	double t, w, d;
	int a;
	Point point;

	public Thread1(Point point, double t, double w, int a, double d) {

		this.point = point;
		this.t = t;
		this.w = w;
		this.a = a;
		this.d = d;

	}

	public void run() {
		double x, y;
		x = t * 100;
		// System.out.println(d);
		y = a * Math.sin(w * t + d);
		point.setLocation(x, y);
	}

	public Point getPoint() {
		return point;
	}

}
