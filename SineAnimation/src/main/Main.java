package main;

import fps_Frame.FPSFrame;

public class Main {

	public Main() {
		RenderPanel p = new RenderPanel();
		FPSFrame f = new FPSFrame("Animated Sine", p, 800, 600, 10);
		f.setVisible(true);
	}

	public static void main(String args[]) {
		new Main();
	}
}
