package main;

import java.awt.*;
import javax.swing.*;
import graphics.*;
import tools.*;

@SuppressWarnings("serial")
public class RenderPanel extends JPanel {
	Sine sine;
	int a, ac = 15;
	double d, f, speed = 0.02;
	GraphicContainer g;
	final int radius = 3, startX = 50, startY = 250;
	private JSlider sliderA;
	private JSlider sliderD;
	private JSlider sliderF;
	private JLabel lblAmplitude;
	private JLabel lblFrequenz;
	private JLabel lblGeschwindigkeit;
	private JSlider sliderAC;
	private JLabel lblGenauigkeit;

	public RenderPanel() {
		setLayout(null);
		add(getSliderA());
		add(getSliderF());
		add(getSliderD());
		add(getLblAmplitude());
		add(getLblFrequenz());
		add(getLblGeschwindigkeit());
		add(getSliderAC());
		add(getLblGenauigkeit());
		a = 150;
		f = 1.0;
		sine = new Sine(f, a);
	}

	protected void paintComponent(Graphics g2) {
		g = new GraphicContainer((Graphics2D) g2);
		tick();
		sine.tick(f, a, ac, speed);

		g2.clearRect(0, 0, 1000, 1000);
		for (int i = 0; i < sine.getDots().size(); i++) {
			g.drawBall((int) (sine.getDots().get(i).getX()) + startX, (int) (sine.getDots().get(i).getY()) + startY,
					radius);
		}

	}

	public void tick() {
		a = (int) (150 * sliderA.getValue() / 100) + 20;
		f = 1.0 * (sliderF.getValue() + 50) * 2 / 100;
		speed = 0.02 * (sliderD.getValue() + 20) * 2 / 100;
		ac = 15 * (sliderAC.getValue() + 20) / 100;
	}

	public JSlider getSliderA() {
		if (sliderA == null) {
			sliderA = new JSlider();
			sliderA.setBounds(12, 499, 185, 26);
		}
		return sliderA;
	}

	public JSlider getSliderD() {
		if (sliderD == null) {
			sliderD = new JSlider();
			sliderD.setBounds(406, 499, 185, 26);
		}
		return sliderD;
	}

	public JSlider getSliderF() {
		if (sliderF == null) {
			sliderF = new JSlider();
			sliderF.setBounds(209, 499, 185, 26);
			sliderF.setValue(0);
		}
		return sliderF;
	}

	public JLabel getLblAmplitude() {
		if (lblAmplitude == null) {
			lblAmplitude = new JLabel("Amplitude");
			lblAmplitude.setBounds(81, 470, 142, 16);
		}
		return lblAmplitude;
	}

	public JLabel getLblFrequenz() {
		if (lblFrequenz == null) {
			lblFrequenz = new JLabel("Frequenz");
			lblFrequenz.setBounds(272, 470, 56, 16);
		}
		return lblFrequenz;
	}

	public JLabel getLblGeschwindigkeit() {
		if (lblGeschwindigkeit == null) {
			lblGeschwindigkeit = new JLabel("Geschwindigkeit");
			lblGeschwindigkeit.setBounds(452, 470, 110, 16);
		}
		return lblGeschwindigkeit;
	}

	public JSlider getSliderAC() {
		if (sliderAC == null) {
			sliderAC = new JSlider();
			sliderAC.setBounds(603, 499, 185, 26);
		}
		return sliderAC;
	}

	public JLabel getLblGenauigkeit() {
		if (lblGenauigkeit == null) {
			lblGenauigkeit = new JLabel("Genauigkeit");
			lblGenauigkeit.setBounds(660, 470, 100, 16);
		}
		return lblGenauigkeit;
	}
}
