import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class KI_Mutter extends Thread {

	public int SCHERGEN_MASSE = 1000;
	String lösungsweg = "";

	public void run() {

		boolean finish = false;
		while (!finish) {
			ArrayList<KI_Scherge> schergen = new ArrayList<KI_Scherge>();

			for (int i = 0; i < SCHERGEN_MASSE; i++) {
				schergen.add(new KI_Scherge());
			}
			for (int i = 0; i < schergen.size(); i++) {
				schergen.get(i).start();
			}

			while (!finish) {
				for (int i = 0; i < schergen.size(); i++) {
					if (!schergen.get(i).isRunning()) {
						lösungsweg = schergen.get(i).getLösungsweg();
						finish = true;
						System.out.println(i);
						break;
					}
				}
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			for (int i = 0; i < schergen.size(); i++) {
				schergen.get(i).shutdown();
			}
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		BufferedWriter w;
		try {
			w = new BufferedWriter(new FileWriter(new File("./src/resource/Lösungsweg.srcki"), false));
			w.write(lösungsweg);
			w.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
