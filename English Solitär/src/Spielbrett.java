import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class Spielbrett extends JPanel {
	KI_Mutter mutter;
	private Feld[] felder;
	JButton reset, finish4me, btnShow;
	JLabel stifte_remain;
	private int amount_Stifte = 0;
	private boolean finished = false, show = false;

	public Spielbrett() {
		setup();
		Thread t = new Thread() {
			@Override
			public void run() {
				int first = -1, middle = -1, second = -1, amount_clicked = 0, direc = -1;
				while (true) {
					if (!show) {
						amount_Stifte = 0;
						for (int i = 0; i < felder.length; i++) {
							if (felder[i].holdStift())
								amount_Stifte++;
						}

						for (int i = 0; i < felder.length; i++) {
							if (felder[i].isClicked() && amount_clicked < 2) {
								if (first != i && second != i) {
									felder[i].setC(Color.green);
									amount_clicked++;

									if (amount_clicked == 1 && first != i) {
										if (felder[i].holdStift()) {
											first = i;
										} else {
											amount_clicked = 0;
										}
									} else {
										second = i;
									}
								}
							} else {
								felder[i].setClicked(false);
								felder[i].setC(Color.white);
							}
						}
						direc = -1;
						if (first > -1 && second > -1) {
							if (felder[first].getY() == felder[second].getY()) {
								if (felder[first].getX() - felder[second].getX() == 2 * Feld.sz) {
									direc = 2;
								} else if (felder[second].getX() - felder[first].getX() == 2 * Feld.sz) {
									direc = 0;
								} else {
									direc = 4;
								}
							} else if (felder[first].getX() == felder[second].getX()) {
								if (felder[first].getY() - felder[second].getY() == 2 * Feld.sz) {
									direc = 1;
								} else if (felder[second].getY() - felder[first].getY() == 2 * Feld.sz) {
									direc = 3;
								} else {
									direc = 4;
								}
							} else {
								direc = 4;
							}
						}
						if (direc > -1) {
							int x = felder[first].getX(), y = felder[first].getY();

							switch (direc) {
							case (0):
								x += Feld.sz;
								break;
							case (1):
								y -= Feld.sz;
								break;
							case (2):
								x -= Feld.sz;
								break;
							case (3):
								y += Feld.sz;
								break;
							case (4):
								for (int i = 0; i < felder.length; i++) {
									felder[i].setClicked(false);
								}
								first = -1;
								second = -1;
								middle = -1;
								amount_clicked = 0;
								break;

							}

							if (direc != 4) {
								for (int i = 0; i < felder.length; i++) {
									if (felder[i].getX() == x && felder[i].getY() == y) {
										middle = i;
									}
								}

								if (felder[first].holdStift() && felder[middle].holdStift()
										&& !felder[second].holdStift()) {

									felder[first].setHoldStift(false);
									felder[middle].setHoldStift(false);
									felder[second].setHoldStift(true);
									for (int i = 0; i < felder.length; i++) {
										felder[i].setClicked(false);
									}
									first = -1;
									second = -1;
									middle = -1;
									amount_clicked = 0;
								}
							}
						}

						if (amount_clicked > 1) {
							for (int i = 0; i < felder.length; i++) {
								felder[i].setClicked(false);
							}
						}

						getStifteRemain();
						repaint();
						try {
							Thread.sleep(1);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						
						// --------------------------------------------------------------------------------------------
						BufferedReader r = null;
						try {
							r = new BufferedReader(new FileReader(new File("./src/resource/Lösungsweg.srcki")));
						} catch (FileNotFoundException e1) {
							e1.printStackTrace();
						}
						String lösungsweg[] = null;
						try {
							lösungsweg = r.readLine().split(";");
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						for (int turn = 0; turn < lösungsweg.length; turn++) {
							direc=-1;
							String zug[] = lösungsweg[turn].split(",");

							felder[Integer.parseInt(zug[0])].setClicked(true);
							felder[Integer.parseInt(zug[1])].setClicked(true);

							amount_Stifte = 0;
							for (int i = 0; i < felder.length; i++) {
								if (felder[i].holdStift())
									amount_Stifte++;
							}

							for (int i = 0; i < felder.length; i++) {
								if (felder[i].isClicked() && amount_clicked < 2) {
									if (first != i && second != i) {
										felder[i].setC(Color.green);
										amount_clicked++;

										if (amount_clicked == 1 && first != i) {
											if (felder[i].holdStift()) {
												first = i;
											} else {
												amount_clicked = 0;
											}
										} else {
											second = i;
										}
									}
								} else {
									felder[i].setClicked(false);
									felder[i].setC(Color.white);
								}
							}
							if (first > -1 && second > -1) {
								if (felder[first].getY() == felder[second].getY()) {
									if (felder[first].getX() - felder[second].getX() == 2 * Feld.sz) {
										direc = 2;
									} else if (felder[second].getX() - felder[first].getX() == 2 * Feld.sz) {
										direc = 0;
									} else {
										direc = 4;
									}
								} else if (felder[first].getX() == felder[second].getX()) {
									if (felder[first].getY() - felder[second].getY() == 2 * Feld.sz) {
										direc = 1;
									} else if (felder[second].getY() - felder[first].getY() == 2 * Feld.sz) {
										direc = 3;
									} else {
										direc = 4;
									}
								} else {
									direc = 4;
								}
							}
							if (direc > -1) {
								int x = felder[first].getX(), y = felder[first].getY();

								switch (direc) {
								case (0):
									x += Feld.sz;
									break;
								case (1):
									y -= Feld.sz;
									break;
								case (2):
									x -= Feld.sz;
									break;
								case (3):
									y += Feld.sz;
									break;
								case (4):
									for (int i = 0; i < felder.length; i++) {
										felder[i].setClicked(false);
									}
									first = -1;
									second = -1;
									middle = -1;
									amount_clicked = 0;
									break;

								}

								if (direc != 4) {

									for (int i = 0; i < felder.length; i++) {
										if (felder[i].getX() == x && felder[i].getY() == y) {
											middle = i;
										}
									}

									if (felder[first].holdStift() && felder[middle].holdStift()
											&& !felder[second].holdStift()) {
										felder[first].setHoldStift(false);
										felder[middle].setHoldStift(false);
										felder[second].setHoldStift(true);
										for (int i = 0; i < felder.length; i++) {
											felder[i].setClicked(false);
										}
										first = -1;
										second = -1;
										middle = -1;
										amount_clicked = 0;
									}
								}
							}

							if (amount_clicked > 1) {
								for (int i = 0; i < felder.length; i++) {
									felder[i].setClicked(false);
								}
							}

							getStifteRemain();
							repaint();
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		t.start();

	}

	@Override
	protected void paintComponent(Graphics g) {

		g.setColor(Color.black);
		g.fillRect(0, 0, 1000, 1000);
		for (int i = 0; i < felder.length; i++) {
			felder[i].draw(g);
		}
		if (finished) {
			g.setColor(Color.green);
		} else {
			g.setColor(Color.red);
		}
		g.fillRect(10 * Feld.sz, 8 * Feld.sz, Feld.sz, Feld.sz / 3);

	}

	private JButton getBtnReset() {
		if (reset == null) {
			reset = new JButton("Reset");
			reset.setBounds(2 * Feld.sz, (int) (10.5 * Feld.sz), 150, 30);
			reset.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					finished = false;
					for (int i = 0; i < felder.length; i++) {
						felder[i].setClicked(false);
						felder[i].setHoldStift(true);
					}
					felder[22].setHoldStift(false);
					amount_Stifte = felder.length - 2;
				}
			});
		}
		return reset;
	}

	private JButton getBtnFinish4me() {
		if (finish4me == null) {
			finish4me = new JButton("Finish for me");
			finish4me.setBounds(2 * Feld.sz, (int) (8.5 * Feld.sz), 150, 30);

			finish4me.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					mutter = new KI_Mutter();
					mutter.start();
					try {
						mutter.join();
						finished = true;
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
			});
		}
		return finish4me;
	}

	private JButton getBtnShow() {
		if (btnShow == null) {
			btnShow = new JButton("Show reasult");
			btnShow.setBounds(2 * Feld.sz, (int) (9.5 * Feld.sz), 150, 30);
			btnShow.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					show = true;
				}
			});
		}
		return btnShow;
	}

	private JLabel getStifteRemain() {
		if (stifte_remain == null) {
			stifte_remain = new JLabel("Remain " + amount_Stifte);
			stifte_remain.setSize(200, 20);
			stifte_remain.setForeground(Color.white);
			stifte_remain.setLocation(9 * Feld.sz, 11 * Feld.sz);
		} else {
			stifte_remain.setText("Remain " + amount_Stifte);
		}
		return stifte_remain;
	}

	private void setup() {
		this.setLayout(null);
		felder = new Feld[45];
		Feld.sz = 60;
		Feld.sz_STIFT = (int) (Feld.sz * 0.8);
		this.add(getStifteRemain());
		this.add(getBtnReset());
		this.add(getBtnFinish4me());
		this.add(getBtnShow());
		for (int j = 0; j < 5; j++) {
			for (int i = 0; i < 9; i++) {
				felder[i + j * 9] = new Feld();
				int x = i;
				int y = 0;
				while (x > 2) {
					y++;
					x -= 3;
				}

				int x1 = 0, y1 = 0;

				switch (j) {
				case 0:
					x1 = 3;
					break;
				case 1:
					x1 = 0;
					y1 = 3;
					break;
				case 2:
					x1 = 3;
					y1 = 3;
					break;
				case 3:
					x1 = 6;
					y1 = 3;
					break;
				case 4:
					x1 = 3;
					y1 = 6;
					break;
				}

				int fx, fy, index;
				fx = (x1 + x + 2) * Feld.sz;
				fy = (y + y1 + 2) * Feld.sz;
				index = i + j * 9;

				felder[index].setBounds(fx, fy, Feld.sz, Feld.sz);
				this.add(felder[index]);
			}
		}

		for (int i = 0; i < 44; i++) {
			int ii = i;
			if (i >= 22) {
				ii++;
			}
			felder[ii].setHoldStift(true);
		}

	}

	public Feld getFeld(int i) {
		return felder[i];
	}

	public void setFeld(int i, Feld felder) {
		this.felder[i] = felder;
	}

	public void clickFeld(int i) {
		this.felder[i].setClicked(true);
	}

	public Feld[] getFelder() {
		return this.felder;
	}

	public int getAmount_Stifte() {
		return amount_Stifte;
	}

}
