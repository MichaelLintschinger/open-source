import java.awt.Color;

public class MathMap {

	Feld[] felder = new Feld[45];
	int direc = -1, amount_Stifte = 0, amount_clicked = -1, first = -1, second = -1, middle = -1;

	public MathMap() {

		for (int j = 0; j < 5; j++) {
			for (int i = 0; i < 9; i++) {
				felder[i + j * 9] = new Feld();
				int x = i;
				int y = 0;
				while (x > 2) {
					y++;
					x -= 3;
				}

				int x1 = 0, y1 = 0;

				switch (j) {
				case 0:
					x1 = 3;
					break;
				case 1:
					x1 = 0;
					y1 = 3;
					break;
				case 2:
					x1 = 3;
					y1 = 3;
					break;
				case 3:
					x1 = 6;
					y1 = 3;
					break;
				case 4:
					x1 = 3;
					y1 = 6;
					break;
				}

				int fx, fy, index;
				fx = (x1 + x + 2) * Feld.sz;
				fy = (y + y1 + 2) * Feld.sz;
				index = i + j * 9;

				felder[index].setBounds(fx, fy, Feld.sz, Feld.sz);
			}
		}

		for (int i = 0; i < 44; i++) {
			int ii = i;
			if (i >= 22) {
				ii++;
			}
			felder[ii].setHoldStift(true);
		}

		Thread t = new Thread() {
			public void run() {
				while (true) {

					amount_Stifte = 0;
					for (int i = 0; i < felder.length; i++) {
						if (felder[i].holdStift())
							amount_Stifte++;
					}

					for (int i = 0; i < felder.length; i++) {
						if (felder[i].isClicked() && amount_clicked < 2) {
							if (first != i && second != i) {
								felder[i].setC(Color.green);
								amount_clicked++;

								if (amount_clicked == 1 && first != i) {
									if (felder[i].holdStift()) {
										first = i;
									} else {
										amount_clicked = 0;
									}
								} else {
									second = i;
								}
							}
						} else {
							felder[i].setClicked(false);
							felder[i].setC(Color.white);
						}
					}
					direc = -1;
					if (first > -1 && second > -1) {
						if (felder[first].getY() == felder[second].getY()) {
							if (felder[first].getX() - felder[second].getX() == 2 * Feld.sz) {
								direc = 2;
							} else if (felder[second].getX() - felder[first].getX() == 2 * Feld.sz) {
								direc = 0;
							} else {
								direc = 4;
							}
						} else if (felder[first].getX() == felder[second].getX()) {
							if (felder[first].getY() - felder[second].getY() == 2 * Feld.sz) {
								direc = 1;
							} else if (felder[second].getY() - felder[first].getY() == 2 * Feld.sz) {
								direc = 3;
							} else {
								direc = 4;
							}
						} else {
							direc = 4;
						}
					}
					if (direc > -1) {
						int x = felder[first].getX(), y = felder[first].getY();

						switch (direc) {
						case (0):
							x += Feld.sz;
							break;
						case (1):
							y -= Feld.sz;
							break;
						case (2):
							x -= Feld.sz;
							break;
						case (3):
							y += Feld.sz;
							break;
						case (4):
							for (int i = 0; i < felder.length; i++) {
								felder[i].setClicked(false);
							}
							first = -1;
							second = -1;
							middle = -1;
							amount_clicked = 0;
							break;

						}

						if (direc != 4) {
							for (int i = 0; i < felder.length; i++) {
								if (felder[i].getX() == x && felder[i].getY() == y) {
									middle = i;
								}
							}

							if (felder[first].holdStift() && felder[middle].holdStift()
									&& !felder[second].holdStift()) {

								felder[first].setHoldStift(false);
								felder[middle].setHoldStift(false);
								felder[second].setHoldStift(true);
								for (int i = 0; i < felder.length; i++) {
									felder[i].setClicked(false);
								}
								first = -1;
								second = -1;
								middle = -1;
								amount_clicked = 0;
							}
						}
					}

					if (amount_clicked > 1) {
						for (int i = 0; i < felder.length; i++) {
							felder[i].setClicked(false);
						}
					}
				}
			}
		};
		t.start();
	}

	public Feld getFeld(int i) {
		return felder[i];
	}

	public int felderLength() {
		return felder.length;
	}

	public void clickFeld(int i) {
		felder[i].setClicked(true);
	}

	public int getAmount_Stifte() {
		return amount_Stifte;
	}
}
