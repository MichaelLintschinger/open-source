import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import graphics.GraphicContainer;

@SuppressWarnings("serial")
public class Feld extends Component {
	public static int sz;
	public static int sz_STIFT;
	private boolean clicked, holdStift;
	private Color c = Color.white;

	public Feld() {
		setSize(sz, sz);
		setFocusable(true);
		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {

			}

			@Override
			public void mouseExited(MouseEvent arg0) {

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				trigger();
			}

			@Override
			public void mouseReleased(MouseEvent arg0) {

			}

		});
	}

	public void trigger() {
		clicked = true;
	}

	@SuppressWarnings("static-access")
	public void draw(Graphics g) {
		setSize(sz, sz);
		g.setColor(Color.white);
		g.drawRect(this.getX(), this.getY(), this.getWidth(), this.getHeight());
		g.setColor(c);
		g.drawRect(this.getX() + 1, this.getY() + 1, this.getWidth() - 2, this.getHeight() - 2);
		g.drawRect(this.getX() + 2, this.getY() + 2, this.getWidth() - 4, this.getHeight() - 4);
		if (holdStift) {
			GraphicContainer.drawBall(new Point(this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2),
					this.sz_STIFT / 2, Color.white, g);
		}
	}

	public boolean entered(Point p) {
		if (this.getX() < p.getX() && this.getY() < p.getY() && this.getX() + this.getWidth() > p.getX()
				&& this.getY() + this.getHeight() > p.getY()) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isClicked() {
		return clicked;
	}

	public void setClicked(boolean clicked) {
		this.clicked = clicked;
	}

	public boolean holdStift() {
		return holdStift;
	}

	public void setHoldStift(boolean i) {
		holdStift = i;
	}

	public Color getC() {
		return c;
	}

	public void setC(Color c) {
		this.c = c;
	}

}