import java.util.concurrent.ThreadLocalRandom;

public class KI_Scherge extends Thread {

	private boolean finished = false;
	private String lösungsweg = "";

	public KI_Scherge() {
	}

	@Override
	public void run() {
		MathMap map = new MathMap();
		while (!finished) {

			int direc = -1, x, y, x1, y1, first = -1, second = -1, middle = -1;
			while (true) {
				while (true) {
					first = ThreadLocalRandom.current().nextInt(0, 45);
					if (map.getFeld(first).holdStift()) {
						break;
					}
				}
				direc = ThreadLocalRandom.current().nextInt(0, 4);

				x = map.getFeld(first).getX();
				y = map.getFeld(first).getY();
				x1 = x;
				y1 = y;

				switch (direc) {
				case (0):
					x += 2 * Feld.sz;
					x1 += Feld.sz;
					break;
				case (1):
					y -= 2 * Feld.sz;
					y1 -= Feld.sz;
					break;
				case (2):
					x -= 2 * Feld.sz;
					x1 -= Feld.sz;
					break;
				case (3):
					y += 2 * Feld.sz;
					y1 += Feld.sz;
					break;
				}
				int l = 0;
				for (int i = 0; i < map.felderLength(); i++) {
					if (map.getFeld(i).getX() == x && map.getFeld(i).getY() == y) {
						l++;
						second = i;
					} else if (map.getFeld(i).getX() == x1 && map.getFeld(i).getY() == y1) {
						middle = i;
						l++;
					}
					if (l >= 2) {
						break;
					}

				}
				if (first > -1 && middle > -1 && second > -1) {
					if (map.getFeld(first).holdStift() && map.getFeld(middle).holdStift()
							&& !map.getFeld(second).holdStift()) {
						break;
					}
				}

			}

			map.clickFeld(first);
			lösungsweg += first + ",";
			map.clickFeld(second);
			lösungsweg += second + ";";

			if (map.getAmount_Stifte() == 1) {
				finished = true;
			}
		}
	}

	public boolean isRunning() {
		return !finished;
	}

	public void shutdown() {
		finished = true;
	}

	public String getLösungsweg() {
		return lösungsweg;
	}

}
