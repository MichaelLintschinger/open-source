import javax.swing.JFrame;

public class FR {

	public FR() {
		Spielbrett b = new Spielbrett();
		JFrame f = new JFrame();
		f.setSize(800, 800);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setTitle("British Solitaer");
		f.add(b);
		f.setLocationRelativeTo(null);
		f.setResizable(false);
		f.setVisible(true);
	}

	public static void main(String[] args) {
		new FR();
	}

}
