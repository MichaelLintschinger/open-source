
package main;

import fps_Frame.FPSFrame;

public class Main {
	static FPSFrame f;
	public Main() {
		RenderPanel p = new RenderPanel();
		f = new FPSFrame("Erste Ableitung", p, 800, 600, 15);
		f.setVisible(true);
	}

	public static void main(String[] args) {
		new Main();
	}

}
