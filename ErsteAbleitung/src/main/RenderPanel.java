package main;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.*;
import javax.swing.*;

@SuppressWarnings("serial")
public class RenderPanel extends JPanel {
	final int maxX = 671, startX = 50, startY = 300;
	ArrayList<Point> drawP, calcP;
	Point mouse;
	public boolean draw;

	public RenderPanel() {
		setFocusable(true);
		addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent arg0) {

			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mousePressed(MouseEvent arg0) {
				draw = true;

			}

			@Override
			public void mouseReleased(MouseEvent arg0) {
				draw = false;
			}

		});
		init();

	}

	private void init() {
		mouse = new Point();
		drawP = new ArrayList<Point>();
		calcP = new ArrayList<Point>();
		for (int i = 0; i < maxX; i++) {
			if (i % 2 == 0) {
				drawP.add(new Point(i, 0));
			} else {
				calcP.add(new Point(i, 0));
			}
		}
	}

	@Override
	protected void paintComponent(Graphics g) {
		clear(g);
		tick();
		for (int i = 0; i < maxX-2; i++) {
			if (i % 2 == 0) {
				g.setColor(Color.red);
				g.drawLine((int) (drawP.get(i / 2).getX()) + startX, (int) (drawP.get(i / 2).getY()) + startY,
						(int) (drawP.get((i + 2) / 2).getX()) + startX, (int) (drawP.get((i + 2) / 2).getY()) + startY);

			} else {
				g.setColor(Color.blue);
				g.drawLine((int) (calcP.get((i - 2) / 2).getX()) + startX,
						(int) (calcP.get((i - 2) / 2).getY()) + startY, (int) (calcP.get(i / 2).getX()) + startX,
						(int) (calcP.get(i / 2).getY()) + startY);
			}
		}
	}

	private Point getMouse() {

		int xf[] = new int[2];
		xf[0] = 0;
		xf[1] = 0;

		xf[0] = (int) Main.f.getLocation().getX();
		xf[1] = (int) Main.f.getLocation().getY();

		int xm[] = new int[2];
		xm[0] = 0;
		xm[1] = 0;

		xm[0] = (int) MouseInfo.getPointerInfo().getLocation().getX() - xf[0] - 3;
		xm[1] = (int) MouseInfo.getPointerInfo().getLocation().getY() - xf[1] - 31;

		// System.out.println(xm[0] + ", " + xm[1]);

		return new Point(xm[0], xm[1]);
	}

	private void tick() {
		mouse = getMouse();
		if (draw && mouse.getX() > 50) {
			try {
				drawP.set((int) ((mouse.getX() - startX) / 2),
						new Point((int) (mouse.getX() - startX), (int) (mouse.getY() - startY)));
			} catch (IndexOutOfBoundsException e) {
			}
			for (int i = 0; i < calcP.size(); i++) {
				calcP.set(i, new Point((int) (calcP.get(i).getX()),
						(int) (drawP.get(i + 1).getY() - drawP.get(i).getY())));
			}
		}

	}

	private void clear(Graphics g) {
		g.clearRect(0, 0, 850, 650);
		g.drawLine(350, 50, 350, 500);
		g.drawLine(50, 300, 720, 300);
	}

}
