package main;

import fps_Frame.FPSFrame;

public class Start{

	public Start() {
		RenderPanel p = new RenderPanel();
		FPSFrame f = new FPSFrame("SineAnimation2.0", p, 800, 800, 20);
		f.setVisible(true);
	}

	public static void main(String[] args) {
		new Start();
	}

}
