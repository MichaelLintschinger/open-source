package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;

import graphics.GraphicContainer;
import tools.Sine;

@SuppressWarnings("serial")
public class RenderPanel extends JPanel {
	private final int A = 100;
	private final double f = 1, phi = Math.PI * 2;
	Sine sine1, sine2;
	Point point;
	ArrayList<Point> points;
	private JSlider sliderA;
	private JLabel lblA;
	private JLabel lblPhi;
	private JSlider sliderPhi;
	private JLabel lblFrequenz;
	private JSlider sliderF;

	public RenderPanel() {
		points = new ArrayList<Point>();
		setLayout(null);
		add(getSliderA());
		add(getLblA());
		add(getLblPhi());
		add(getSliderPhi());
		add(getLblFrequenz());
		add(getSliderF());
		point = new Point(0, 0);
		sine1 = new Sine(350, 200, 1, 0, A, false, 300);
		sine2 = new Sine(200, 650, 1, 1, A, true, 300);
	}

	@Override
	protected void paintComponent(Graphics g) {
		tick();
		if (points.size() > 500) {
			shift();
			while (true) {
				try {
					points.remove(500);
				} catch (Exception e) {
					break;
				}
			}
		}
		g.clearRect(0, 0, 1000, 700);
		GraphicContainer c = new GraphicContainer((Graphics2D) g);

		for (int i = 0; i < sine1.getPoints().size(); i++) {
			c.drawBall((int) (sine1.getPoints().get(i).getX() + sine1.getX()),
					(int) (sine1.getY() - sine1.getPoints().get(i).getY()), 1, Color.red);
			c.drawBall((int) (sine2.getPoints().get(i).getX() + sine2.getX()),
					(int) (sine2.getY() - sine2.getPoints().get(i).getY()), 1, Color.blue);
		}
		for (int i = 0; i < points.size(); i++) {
			c.drawBall((int) (points.get(i).getX()), (int) (points.get(i).getY()), 2, Color.gray);
		}

		c.drawPoint((int) sine1.getX(), (int) (sine1.getY() - sine1.getPoints().get(0).getY()), Color.red, 10);
		c.drawPoint((int) sine2.getX() + (int) sine2.getPoints().get(0).getX(),
				(int) (sine2.getY() - sine2.getPoints().get(sine2.getPoints().size() - 1).getY()), Color.blue, 10);

		g.setColor(Color.red);
		g.drawLine((int) point.getX(), (int) point.getY(), (int) sine1.getX(),
				(int) (sine1.getY() - sine1.getPoints().get(0).getY()));

		g.setColor(Color.blue);
		g.drawLine((int) point.getX(), (int) point.getY(), (int) sine2.getX() + (int) sine2.getPoints().get(0).getX(),
				(int) (sine2.getY() - sine2.getPoints().get(sine2.getPoints().size() - 1).getY()));

		c.drawBall((int) point.getX(), (int) point.getY(), 5, Color.black);
	}

	public void tick() {
		sine2.setA(A * sliderA.getValue() / 100);
		sine2.setF(1+sliderF.getValue() / 10);
		sine2.setPhi(phi * sliderPhi.getValue() / 100);

		sine1.tick();
		sine2.tick();
		point.setLocation(sine2.getX() + sine2.getPoints().get(0).getX(),
				sine1.getY() - sine1.getPoints().get(0).getY());
		points.add(new Point((int) point.getX(), (int) point.getY()));

	}

	private void shift() {
		for (int i = 1; i < points.size(); i++) {
			points.set(i - 1, points.get(i));
		}
	}

	public JSlider getSliderA() {
		if (sliderA == null) {
			sliderA = new JSlider();
			sliderA.setBounds(363, 427, 421, 26);
		}
		return sliderA;
	}

	public JLabel getLblA() {
		if (lblA == null) {
			lblA = new JLabel("Amplitude");
			lblA.setBounds(543, 398, 96, 16);
		}
		return lblA;
	}

	public JLabel getLblPhi() {
		if (lblPhi == null) {
			lblPhi = new JLabel("Phi");
			lblPhi.setBounds(543, 466, 56, 16);
		}
		return lblPhi;
	}

	public JSlider getSliderPhi() {
		if (sliderPhi == null) {
			sliderPhi = new JSlider();
			sliderPhi.setBounds(363, 495, 421, 26);
		}
		return sliderPhi;
	}

	public JLabel getLblFrequenz() {
		if (lblFrequenz == null) {
			lblFrequenz = new JLabel("Frequenz");
			lblFrequenz.setBounds(543, 534, 56, 16);
		}
		return lblFrequenz;
	}

	public JSlider getSliderF() {
		if (sliderF == null) {
			sliderF = new JSlider();
			sliderF.setBounds(363, 563, 421, 26);
		}
		return sliderF;
	}
}
