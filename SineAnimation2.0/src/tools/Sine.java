package tools;

import java.awt.Point;
import java.util.ArrayList;

public class Sine {

	protected ArrayList<Point> points;
	private double w, phi,phistat;
	private int A, length, x, y;

	boolean vert;

	public Sine(int x, int y, double f, double phi, int A, boolean vert, int length) {
		points = new ArrayList<Point>();
		for (int i = 0; i < length; i++) {
			points.add(new Point(0, 0));
		}
		this.x = x;
		this.y = y;
		this.A = A;
		this.phi = phi;
		this.w = f * 2 * Math.PI;
		this.vert = vert;
		this.length = length;
	}

	public void tick() {

		final int div = 15;
		phistat += (1.0 / div);
		if (phistat > Math.PI * 2) {
			phistat = 0;
		}
		Thread th[] = new Thread[length];
		for (int i = 0; i < th.length; i++) {
			final int t = i;

			th[i] = new Thread() {
				public void run() {
					if (!vert) {
						points.set(t, new Point(t, (int) (A * Math.sin(w * t / length + phi+phistat))));
					} else {
						points.set(t, new Point((int) (A * Math.sin(w * t / length + phi+phistat)), t));
					}
				}
			};
			th[i].start();

		}
		for (int i = 0; i < th.length; i += 3) {
			try {
				th[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<Point> getPoints() {
		return points;
	}

	public void setPoints(ArrayList<Point> points) {
		this.points = points;
	}

	public double getW() {
		return w;
	}

	public void setF(double w) {
		this.w = w * 2 * Math.PI;
	}

	public double getPhi() {
		return phi;
	}

	public void setPhi(double phi) {
		this.phi = phi;
	}

	public int getA() {
		return A;
	}

	public void setA(int a) {
		A = a;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isVert() {
		return vert;
	}

	public void setVert(boolean vert) {
		this.vert = vert;
	}

}
