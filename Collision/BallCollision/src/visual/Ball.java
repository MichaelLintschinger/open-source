package visual;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;

public class Ball extends JComponent {
    private double vectorx, vectory,//Bewegungsvektoren
            x, y;//Position
    private Color color;
    private static final double REIBUNG = 0;

    public Ball(Point pos, int r) {
        this.x = pos.x - r;
        this.y = pos.y - r;
        setLocation((int) x, (int) y);
        setR(r);
        this.vectorx = 0;
        this.vectory = 0;

    }

    public Ball(Ball b) {
        this.x = b.x;
        this.y = b.y;
        setLocation((int) b.x, (int) b.y);
        setR(b.getR());
        this.vectorx = b.getVectorx();
        this.vectory = b.getVectory();
        setColor(b.getColor());
    }

    public int getR() {//Radius
        return getWidth() / 2;
    }

    public void setR(int r) {
        x -= (r - getR());
        y -= (r - getR());
        setSize(2 * r, 2 * r);
        setLocation((int) x, (int) y);//Zum visuellen Update
    }

    public double getVectorx() {
        return vectorx;
    }

    public double getVectory() {
        return vectory;
    }

    public void setVector(double x, double y) {
        this.vectorx = x;
        this.vectory = y;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    //Pi*r^2
    public double getMass() {
        return getR() * getR() * Math.PI;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(getColor());
        g2.fillOval(0, 0, getWidth(), getHeight());
        g2.setColor(Color.RED);
        g2.drawLine(getR(), getR(), getR() + (int) (vectorx / 10), getR() + (int) (vectory / 10)); // 1/10 -> Skallierung

    }

    //keep Object in Screen
    public void calcSideCollision() {
        if (x < 0) {
            vectorx *= -1;
            x *= -1;
        } else if (x > getParent().getSize().width - 2 * getR()) {
            vectorx *= -1;
            x -= x - (getParent().getSize().width - 2 * getR());
        }
        if (y < 0) {
            vectory *= -1;
            y *= -1;
        } else if (y > getParent().getSize().height - 2 * getR()) {
            vectory *= -1;
            y -= y - (getParent().getSize().height - 2 * getR());
        }
    }

    //Berechnet die neue Position und stellt dar
    public void tick() {

        final double speed = 0.0025,
                length = Math.sqrt(vectorx * vectorx + vectory * vectory);//Länge des Richtungsvektors

        if (length > 0.1) {


            x += speed * vectorx;
            y += speed * vectory;


            double factor = (length - REIBUNG) / length;

            vectory *= factor;
            vectorx *= factor;
        }

        calcSideCollision();

        setLocation((int) x, (int) y);// updated Visuelle Komponenten von java.awt.JComponent

    }

    //Vektor vom eigenen Mittelpunkt zum Mittelpunkt von b
    public Point2D.Double getDifferenceVector(Ball b) {
        return new Point2D.Double((b.x + b.getR()) - (x + getR()), (b.y + b.getR()) - (y + getR()));
    }

    //Länge von getDifferenceVector
    public double getDifferenceDistance(Ball b) {
        Point2D.Double p = getDifferenceVector(b);
        return Math.sqrt(p.x * p.x + p.y * p.y);
    }

    //checkt ob es zu einer Kollision kommt
    public boolean doesCollideWith(Ball b) {
        return getDifferenceDistance(b) <= b.getR() + getR();
    }

    //anwendung der Formel für den elastischen Stoß (siehe Doku)
    public void colliding(Ball b) {
        vectorx = (getMass() * vectorx + b.getMass() * (2 * b.getVectorx() - vectorx)) / (getMass() + b.getMass());
        vectory = (getMass() * vectory + b.getMass() * (2 * b.getVectory() - vectory)) / (getMass() + b.getMass());
    }

}
