package visual;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MainFrame extends JFrame {

    JPanel panel;
    Thread t_BirthCounter;//Neuer Thread zur Erhöhung der Größe des neuen Objekts
    boolean birthThreadRunning = false,//Objekt wird erstellt
     birthDone = true;//Objekt wurde erstellt (wichtig, falls sich das neue Objekt gerade mit einem bestehenden Überschneidet)
    int birthCnt;//Radius neuer Objekte
    public final int MAX_SIZE = 100;//Maximaler Radius

    Point begin, end;//Zur erstellung des Richtungsvektors

    public MainFrame(int width, int height) {
        panel = new JPanel();

        setSize(width, height);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle("LinM Kollision");

        //Hinzufügen neuer Objekte
        panel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {
                startCounter(e.getPoint());
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                try {
                    stopCounter(e.getPoint());
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        panel.setBackground(Color.gray);
        add(panel);
        setVisible(true);


        //Bewegung und Kollision
        new Thread() {
            @Override
            public void run() {
                super.run();
                while (true) {
                    try {
                        for (int i = 0; i < panel.getComponentCount(); i++) {
                            for (int j = i + 1; j < panel.getComponentCount(); j++) {//Kollisionserkennung
                                if (((Ball) panel.getComponent(i)).doesCollideWith(((Ball) panel.getComponent(j)))&&//Kollisionserkennung
                                        (j!=panel.getComponentCount()-1||(birthDone&&j==panel.getComponentCount()-1))) {//growing circles excluded

                                    Ball b = new Ball((Ball) panel.getComponent(i));
                                    ((Ball) panel.getComponent(i)).colliding(((Ball) panel.getComponent(j)));//Kollisionsausführung
                                    ((Ball) panel.getComponent(j)).colliding(b);

                                }
                            }
                            ((Ball) panel.getComponent(i)).tick();//neue Position berechnen
                        }

                        Thread.sleep(2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

    }

    private void startCounter(Point p) {
        if(!birthDone){//neuestes Objekt löschen (ersetzen)
            panel.remove(panel.getComponentCount()-1);
        }

        if (t_BirthCounter == null || !birthThreadRunning) {//erstellen eines neuen Objekts
            begin = p;
            birthCnt = 0;
            birthThreadRunning = true;
            birthDone = false;
            //Größe und Farbe fortlaufend ändern
            t_BirthCounter = new Thread() {
                @Override
                public void run() {
                    try {
                        while (birthThreadRunning) {
                            if (birthCnt == 0) {
                                panel.add(new Ball(p, 0));
                            }
                            //erhöhen oder auf MAX setzen
                            birthCnt += (birthCnt > MAX_SIZE) ? 0 : 1;
                            //neuer Radius
                            ((Ball) panel.getComponent(panel.getComponentCount() - 1)).setR(birthCnt);
                            //Neue Farbe
                            int colorR = (int) (birthCnt * 255.0 / MAX_SIZE);
                            ((Ball) panel.getComponent(panel.getComponentCount() - 1)).setColor(new Color(255 - colorR, 0, 0));

                            Thread.sleep(30);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    super.run();
                }
            };
            t_BirthCounter.start();
        }
    }

    private void stopCounter(Point p) throws InterruptedException {
        end = p;
        birthThreadRunning = false;
        if (t_BirthCounter.isAlive())
            t_BirthCounter.join();
        Ball b = ((Ball) panel.getComponent(panel.getComponentCount() - 1));

        new Thread() {//warten bis das neue Objekt ungehindert Platziert werden kann
            @Override
            public void run() {
                super.run();
                for (int i = 0; i < panel.getComponentCount() - 1; i++) {

                    while (b.doesCollideWith((Ball) panel.getComponent(i))) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        i = 0;
                    }
                }
                ((Ball) panel.getComponent(panel.getComponentCount() - 1)).setVector(p.x - b.getX() - b.getR(), p.y - b.getY() - b.getR());
                //Farbe wechseln von rot auf grün
                int colorR = (int) (b.getR() * 255.0 / MAX_SIZE);
                ((Ball) panel.getComponent(panel.getComponentCount() - 1)).setColor(new Color(0, 255 - colorR, 0));
                birthDone = true;
            }
        }.start();

    }

}
