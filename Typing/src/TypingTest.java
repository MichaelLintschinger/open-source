import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class TypingTest {
    ArrayList<String> list = new ArrayList();
    String currentWord, nextWord;
    boolean[] wrong=new boolean[50];
    int counter = 0;

    public static void main(String[] args) {
        new TypingTest();
    }

    TypingTest() {

        JFrame f = new JFrame("TypingTest");
        f.setSize(500, 300);
        //f.setLayout(null);
        f.setLocationRelativeTo(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try {
            BufferedReader r = new BufferedReader(new FileReader(new File("./resource/words.txt")));
            String line = r.readLine();
            while (line != null) {
                list.add(line);
                line = r.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        currentWord = getRandomWord();
        nextWord = getRandomWord();

        JPanel p = new JPanel();
        p.setLayout(null);
        p.setFocusable(true);

        JLabel l1 = new JLabel("", SwingConstants.CENTER), l2 = new JLabel("", SwingConstants.CENTER);

        l1.setBounds(0, 0, 500, 100);
        l1.setFont(new Font("Serif", Font.PLAIN, 40));
        l1.setText(currentWord);

        l2.setBounds(0, 100, 500, 100);
        l2.setFont(new Font("Serif", Font.PLAIN, 40));
        l2.setText("<html><font color='gray'>" + nextWord + "<font></html>");


        p.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
                if (keyEvent.getKeyChar() == currentWord.toCharArray()[counter]) {
                counter++;
                if (counter == currentWord.length()) {
                    for(int i=0;i<50;i++){
                        wrong[i]=false;
                    }
                    counter = 0;
                    currentWord = nextWord;
                    nextWord = getRandomWord();
                }

            } else {

                wrong[counter]=true;

                //counter++;
            }

                String txtl="<html> <font color='green'>" + currentWord.substring(0, counter) + "</font>" + currentWord.substring(counter) + "</html>";

                l1.setText(txtl);

                l2.setText("<html><font color='gray'>" + nextWord + "<font></html>");
            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        });

        p.add(l1);
        p.add(l2);

        f.add(p);

        f.setVisible(true);
    }

    String getRandomWord() {
        return list.get(ThreadLocalRandom.current().nextInt(0, list.size()));
    }
}
