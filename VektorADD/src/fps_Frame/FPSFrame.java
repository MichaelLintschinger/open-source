package fps_Frame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

/* Created by Michael_Lintschinger on 15.06.2017
 * ~60 fps
 * requests:
 * 		-title
 * 		-JPanel with void paintComponent
 *	 	-Size x
 * 		-Size y
*/
@SuppressWarnings("serial")
public class FPSFrame extends JFrame {
	private int ms;
	private JPanel p1;
	private JPanel[] p2;
	private ArrayList<JPanel> p3;
	Timer timer;

	public FPSFrame(String title, JPanel p, int x, int y, int ms) {
		this.ms = ms;

		setSize(x, y);
		setTitle(title);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		p1 = p;
		add(p1);
		timer = new Timer(ms, e -> p1.repaint());
	}


	public void start() {

		if(ms == 0){
			while(true){
				p1.repaint();
			}
		}else{
			timer.start();
		}
	}

	public FPSFrame(String title, JPanel p[], int x, int y, int ms) {

		Timer timer = new Timer(ms, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < p2.length; i++) {
					p2[i].repaint();
				}
			}
		});
		setSize(x, y);
		setTitle(title);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		p2 = p;
		for (int i = 0; i < p2.length; i++) {
			add(p2[i]);
		}
		timer.start();
	}

	public FPSFrame(String title, ArrayList<JPanel> p, int x, int y, int ms) {

		p3 = p;
		Timer timer = new Timer(ms, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < p2.length; i++) {
					p2[i].repaint();
				}
			}
		});
		setSize(x, y);
		setTitle(title);
		setResizable(false);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		for (int i = 0; i < p2.length; i++) {
			add(p3.get(i));
		}
		timer.start();
	}

}