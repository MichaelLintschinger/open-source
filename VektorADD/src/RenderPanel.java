import tools.ChooseVectorPanel;
import tools.SettingsPanel;
import tools.VectorPanel;
import tools.Vector;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class RenderPanel extends JPanel {
    JLabel cc;
    SettingsPanel settingsPanel;
    private Color background = Color.white, graphColor = Color.black;
    private char[] shortcuts = {'a', 'd', 's'};
    private int speed = 1;
    KeyListener k;
    BufferedImage img;
    ArrayList<Vector> vectors;
    ArrayList<Point> graph, graph2;
    final Point ref = new Point(150, 300);
    Point pic = new Point(0, 0);
    boolean play, start = true;
    int index, counter;
    VectorPanel p = new VectorPanel();
    ChooseVectorPanel pc = new ChooseVectorPanel(vectors);

    public RenderPanel() {
        cc = new JLabel("\u00a9 " + getmfg("Ie_d]ah\u001CHejpo_dejcan"));
        cc.setBounds(640, 580, 200, 20);

        settingsPanel = new SettingsPanel(background, graphColor, shortcuts, speed);
        settingsPanel.setBounds(0, 600, 900, 200);

        setLayout(null);
        vectors = new ArrayList<>();
        graph = new ArrayList<>();
        graph2 = new ArrayList<>();
        Vector.t = 0;

        setFocusable(true);
        pc.setVisible(false);
        p.setVisible(false);
        pc.setLocation(300, 600);
        add(pc);
        add(p);
        add(settingsPanel);
        add(cc);

        update();
        addKeyListener(k);
        p.addOrSet("Add");
        //addVector_Step1();
        setVisible(true);
        settingsPanel.setVisible(false);
        play = true;
    }

    private void update() {
        final char in[] = shortcuts;
        k = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

                p.setBG(background);
                pc.setBG(background);
                char ch = e.getKeyChar();
                if (ch == in[0]) {
                    if (!p.isVisible() && !pc.isVisible() && !settingsPanel.isVisible()) {
                        graph2.clear();
                        graph.clear();
                        p.reset();
                        p.addOrSet("Add");

                        addVector_Step1();
                    }
                } else if (ch == KeyEvent.VK_SPACE) {
                    play = !play;
                } else if (ch == in[1]) {
                    if (!p.isVisible() && vectors.size() > 0 && !pc.isVisible() && !settingsPanel.isVisible()) {
                        graph2.clear();
                        graph.clear();
                        if (vectors.size() == 1) {
                            play = false;
                            p.setVector(vectors.get(0));
                            p.addOrSet("Set");
                            p.setVisible(true);
                        } else {
                            play = false;
                            pc.setVectors(vectors);
                            pc.setVisible(true);
                        }
                    }
                } else if (ch == in[2]) {
                    if (!p.isVisible() && !pc.isVisible() && !settingsPanel.isVisible()) {
                        settingsPanel.activate();
                        play = false;
                    }

                }
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        };
    }

    private void addVector_Step1() {
        play = false;
        if (p.getAddOrSet().equals("Add"))
            p.setS(vectors.size());
        p.setVisible(true);
    }

    private void addVector_Step2() {

        if (p.finished()) {
            if (p.getAddOrSet().equals("Set")) {
                vectors.set(index, p.getVector());
                int xyz = vectors.get(index).getOnTopOf();
                if (xyz == index || xyz >= vectors.size() || xyz <= -1) {
                    vectors.get(index).setOnTopOf((short) -1);
                }
            } else {
                vectors.add(p.getVector());
                int xyz = (int) vectors.get(vectors.size() - 1).getOnTopOf();
                if (xyz == vectors.size() - 1 || xyz >= vectors.size() || xyz < -1) {
                    vectors.get(vectors.size() - 1).setOnTopOf((short) -1);
                }
            }
        }

        p.reset();
        play = true;

    }

    @Override
    protected void paintComponent(Graphics gc) {

        Graphics2D g2 = (Graphics2D) gc;
        int w = this.getWidth();
        int h = this.getHeight();
        if (img == null) {
            img = (BufferedImage) (this.createImage(w, h));
        }
        Graphics2D g = img.createGraphics();
        g2.drawImage(img, null, 0, 0);

        g.setBackground(background);
        g.clearRect(0, 0, 1000, 1000);

        g.setColor(Color.black);
        g.drawLine(0, ref.y, 800, ref.y);
        g.drawLine(ref.x + 250, ref.y, ref.x + 250, ref.y - 200);
        g.drawLine(ref.x + 250, ref.y, ref.x + 250, ref.y + 200);
        g.drawLine(ref.x, ref.y, ref.x, ref.y + 200);
        g.drawLine(ref.x, ref.y, ref.x, ref.y - 200);

        int x = ref.x, y = ref.y;

        g.setColor(graphColor);
        ArrayList<Short> vectorsWithoutEND = new ArrayList();

        for (int i = 0; i < vectors.size(); i++) {
            if (!vectorsWithoutEND.contains(vectors.get(i).getOnTopOf()) && vectors.get(i).getOnTopOf() != -1) {
                vectorsWithoutEND.add(vectors.get(i).getOnTopOf());
            }
        }

        g.setColor(graphColor);
        for (int i = 0; i < graph2.size(); i++) {
            x = graph2.get(i).x;
            y = graph2.get(i).y;
            g.drawLine(x, y, x, y);
        }
        for (int i = 0; i < graph.size(); i++) {
            x = graph.get(i).x;
            y = graph.get(i).y;
            g.drawLine(x, y, x, y);
        }
        for (int i = 0; i < graph.size(); i++) {
            x = graph.get(i).x;
            y = graph.get(i).y;
            g.drawLine(x, y, x, y);
        }

        for (int i = 0; i < vectors.size(); i++) {
            x = ref.x;
            y = ref.y;
            int j = i, ind = vectors.get(j).getOnTopOf();
            ArrayList<Integer> alreadyTaken = new ArrayList<>();
            int err = 0;
            while (ind != -1) {
                ind = vectors.get(j).getOnTopOf();
                if (alreadyTaken.contains(ind)) {
                    err = 1;
                    break;
                }
                alreadyTaken.add(ind);
                if (ind != -1) {
                    x += vectors.get(ind).getPos().x;
                    y += vectors.get(ind).getPos().y;
                }
                j = ind;
            }
            if (err == 0) {
                g.setColor(vectors.get(i).getColor());
                g.drawLine(x, y, vectors.get(i).getPos().x + x, vectors.get(i).getPos().y + y);
            } else {
                vectors.get(i).setOnTopOf((short) -1);
            }
        }

        if (play || start) {
            start = false;
            counter++;


            Vector.t = Vector.t + 0.01;
            for (int i = 0; i < vectors.size(); i++) {
                vectors.get(i).tick();
            }

            if (vectors.size() > 0) {
                if (counter > speed) {
                    counter = 0;
                    for (int i = 0; i < graph.size(); i++) {
                        if (graph.get(i).getX() > 800) {
                            graph.remove(i);
                        }
                    }
                    for (int i = 0; i < graph.size(); i++) {
                        graph.get(i).x++;
                    }

                    ArrayList<Point> pfg = new ArrayList<>();
                    for (int i = 0; i < vectors.size(); i++) {
                        if (vectors.get(i).isVisible()) {
                            x = ref.x;
                            y = ref.y;
                            int j = i, ind = vectors.get(j).getOnTopOf();

                            while (ind != -1) {
                                ind = vectors.get(j).getOnTopOf();
                                if (ind != -1) {
                                    x += vectors.get(ind).getPos().x;
                                    y += vectors.get(ind).getPos().y;
                                }
                                j = ind;
                            }

                            pfg.add(new Point(vectors.get(i).getPos().x + x, vectors.get(i).getPos().y + y));
                        }
                    }
                    for (int i = 0; i < pfg.size(); i++) {
                        graph2.add(pfg.get(i));
                        graph.add(new Point(400, pfg.get(i).y));
                    }

                }
            }
        } else if (p.finished() || p.closed()) {
            addVector_Step2();

        } else if (p.isDelete()) {
            vectors.remove(index);
            play = true;
            p.reset();
            pc.reset();

        } else if (pc.isChosen()) {
            try {
                index = pc.getIndex();
                p.setVector(pc.getVector());
                p.addOrSet("Set");
                pc.reset();
                addVector_Step1();
            } catch (Exception e) {
                pc.setChosen(false);
            }
        } else if (settingsPanel.isVisible()) {
            background = settingsPanel.getBackgroundColor();
            graphColor = settingsPanel.getGraphColor();
            speed = settingsPanel.getSpeed();
            shortcuts = settingsPanel.getShortcuts();
            if (settingsPanel.isDone()) {
                settingsPanel.setVisible(false);
                play = true;
            }
        }
        //System.out.println("play = " + play + ", vectors.size() = " + vectors.size());
    }

    public String getmfg(String in) {
        char[] betw = in.toCharArray();
        for (int i = 0; i < betw.length; i++) {
            betw[i] += 4;
        }
        in = String.valueOf(betw);
        return in;
    }
}
