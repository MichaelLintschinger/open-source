package tools;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VectorPanel extends JPanel {
    Vector vector = new Vector("Vector", (short) 50, 1, new Color(0, 0, 0));
    Color color = Color.white;
    JLabel l_Length, l_Color, l_Title, l_w, l_Header, l_onTopOf, l_visible;
    JButton btn_Add, btn_Cancel, btn_delete;
    ColorChooserButton btn_Color;
    JTextField txt_Length, txt_Title, txt_w, txt_OnTopOf;
    JCheckBox cbx_visible;
    private boolean close = false, finish = false, delete = false;


    final int x = 30, y = 10;


    public VectorPanel() {
        setLayout(null);
        setSize(900, 300);
        setLocation(0, 550);

        l_visible = new JLabel("set track visible");
        l_visible.setBounds(x + 500, y + 50, 200, 20);

        l_onTopOf = new JLabel("Were to set vector");
        l_onTopOf.setBounds(x + 500, y + 80, 200, 20);

        l_Header = new JLabel("Create new vector");
        l_Header.setBounds(200 + x, y, 200, 30);

        l_Title = new JLabel("Title");
        l_Title.setBounds(50 + x, 50 + y, 50, 20);

        l_Color = new JLabel("Color");
        l_Color.setBounds(50 + x, 90 + y, 50, 20);

        cbx_visible = new JCheckBox();
        cbx_visible.setBounds(x + 600, y + 50, 20, 20);

        txt_Title = new JTextField();
        txt_Title.setBounds(90 + x, 48 + y, 100, 25);
        txt_Title.setText(vector.getTitle());

        btn_Color = new ColorChooserButton(new Color(0, 0, 0));
        btn_Color.setBounds(90 + x, 90 + y, 20, 20);

        btn_Add = new JButton("Add");
        btn_Add.setBackground(Color.blue);
        btn_Add.setForeground(Color.white);
        btn_Add.setBounds(50 + x, 120 + y, 100, 30);
        btn_Add.addActionListener(e -> {
            finish = true;
            close = false;
            delete = false;
        });

        btn_Cancel = new JButton("Cancel");
        btn_Cancel.setBackground(Color.yellow);
        btn_Cancel.setForeground(Color.black);
        btn_Cancel.setBounds(180 + x, 120 + y, 100, 30);
        btn_Cancel.addActionListener(e -> {
            finish = false;
            close = true;
            delete = false;
        });

        btn_delete = new JButton("Delete");
        btn_delete.setBackground(Color.red);
        btn_delete.setForeground(Color.white);
        btn_delete.setBounds(310 + x, 120 + y, 100, 30);
        btn_delete.addActionListener(e -> {
            finish = false;
            delete = true;
            close = false;
        });


        l_Length = new JLabel("Length");
        l_Length.setBounds(230 + x, 50 + y, 50, 20);

        txt_Length = new JTextField();
        txt_Length.setBounds(350 + x, 45 + y, 100, 20);
        txt_Length.setText("" + vector.getS());

        l_w = new JLabel("Circuit frequency");
        l_w.setBounds(230 + x, 80 + y, 100, 20);

        txt_w = new JTextField();
        txt_w.setBounds(350 + x, 80 + y, 100, 20);
        txt_w.setText("" + vector.getW());

        txt_OnTopOf = new JTextField();
        txt_OnTopOf.setBounds(x + 620, y + 80, 50, 20);

        add(cbx_visible);
        add(l_visible);
        add(txt_OnTopOf);
        add(l_onTopOf);
        add(btn_delete);
        add(txt_w);
        add(l_w);
        add(txt_Length);
        add(l_Length);
        add(btn_Cancel);
        add(btn_Add);
        add(l_Header);
        add(btn_Color);
        add(l_Title);
        add(txt_Title);
        add(l_Color);

    }

    public void addOrSet(String asdf) {
        btn_Add.setText(asdf);
        if (asdf.equals("Add")) {
            txt_OnTopOf.setText("null");
            btn_delete.setVisible(false);
            vector.setOnTopOf((short) -1);
        } else {
            btn_delete.setVisible(true);
        }

    }

    public Vector getVector() {
        Vector v = new Vector(vector.getTitle(), vector.getS(), vector.getW(), vector.getColor());
        v.setTitle(txt_Title.getText());
        v.setColor(btn_Color.getSelectedColor());
        v.setVisible(cbx_visible.isSelected());
        try {
            v.setOnTopOf(Short.parseShort(txt_OnTopOf.getText()));

        } catch (Exception e) {
            v.setOnTopOf((short) -1);
        }
        try {
            v.setSize(Short.parseShort(txt_Length.getText()));
        } catch (Exception e) {
            v.setSize((short) 50);
        }
        try {
            v.setW(Float.parseFloat(txt_w.getText()));
        } catch (Exception e) {
            v.setW(1);
        }
        return v;
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setBackground(color);
        g2.clearRect(0, 0, 1000, 1000);

    }

    public void setVector(Vector v) {
        vector = new Vector(v.getTitle(), v.getS(), v.getW(), v.getColor());
        vector.setOnTopOf(v.getOnTopOf());
        vector.setVisible(v.isVisible());

        txt_Length.setText("" + vector.getS());
        txt_w.setText("" + vector.getW());
        txt_Title.setText(vector.getTitle());
        btn_Color.setSelectedColor(vector.getColor());
        if ((int) vector.getOnTopOf() >= 0) {
            txt_OnTopOf.setText(vector.getOnTopOf() + "");
        } else {
            txt_OnTopOf.setText("null");
        }
        cbx_visible.setSelected(v.isVisible());

    }

    public boolean closed() {
        return close;
    }

    public boolean finished() {

        return finish;
    }

    public void reset() {
        close = false;
        finish = false;
        delete = false;
        this.setVisible(false);
    }

    public String getAddOrSet() {
        return btn_Add.getText();
    }

    public boolean isDelete() {
        return delete;
    }

    public void setBG(Color c) {
        color = c;

        repaint();
    }

    public void setS(int size) {
        txt_Title.setText("Vector " + (size + 1));
    }
}
