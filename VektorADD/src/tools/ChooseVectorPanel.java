package tools;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ChooseVectorPanel extends JPanel {
    private int index;
    private final int x = 0, y = 0;
    private ArrayList<Vector> vectors;
    private boolean chosen;
    JLabel lbl;
    JTextField txt_getVectors;
    Color color = Color.white;

    public ChooseVectorPanel(ArrayList<Vector> v) {

        setSize(300, 300);

        setLayout(null);
        vectors = v;
        lbl = new JLabel("Choose a Vector to edit by index or name");
        txt_getVectors = new JTextField();
        index = 0;

        lbl.setBounds(x, y, 300, 20);
        txt_getVectors.setBounds(x, y + 20, 100, 20);
        txt_getVectors.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                chosen = true;
            }
        });

        add(lbl);
        add(txt_getVectors);

    }

    public void setVectors(ArrayList<Vector> vectors) {
        this.vectors = vectors;
    }

    public Vector getVector() {
        return vectors.get(index);
    }

    public int getIndex() {
        try {
            index = Integer.parseInt(txt_getVectors.getText());
        } catch (NumberFormatException e) {
            for (int i = 0; i < vectors.size(); i++) {
                if (txt_getVectors.getText().equals(vectors.get(i).getTitle())) {
                    index = i;
                    break;
                }
                index = 0;
            }
        }
        return index;
    }

    public void reset() {
        index = 0;
        setVisible(false);
        chosen = false;
    }

    public void setBG(Color c) {
        color = c;

        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setBackground(color);
        g.clearRect(0, 0, 1000, 1000);
    }

    public boolean isChosen() {
        return chosen;
    }

    public void setChosen(boolean b) {
        chosen = b;
    }
}
