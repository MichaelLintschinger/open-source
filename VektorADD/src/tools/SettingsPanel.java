package tools;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SettingsPanel extends JPanel {
    private Color background, graphColor;
    private char[] shortcuts;
    private int speed;
    private final int x = 10, y = 30;
    private boolean done = false;

    JLabel lbl_background, lbl_graphColor, lbl_SC_add, lbl_SC_settings, lbl_SC_edit, lbl_speed;
    ColorChooserButton btn_background, btn_graphColor;
    KeyButton btn_SC_ADD, btn_SC_SETTINGS, btn_SC_EDIT;
    JTextField txt_Speed;
    JButton btn_Done;

    public SettingsPanel(Color backgroundColor, Color graphColor, char[] sc, int speed) {
        setLayout(null);
        background = backgroundColor;
        this.graphColor = graphColor;
        shortcuts = sc;
        this.speed = speed;

        lbl_background = new JLabel("set Background Color");
        lbl_background.setBounds(x, y, 200, 20);

        lbl_graphColor = new JLabel("set Graph Color");
        lbl_graphColor.setBounds(x + 200, y, 200, 20);

        lbl_SC_add = new JLabel("set Shortcut for 'ADD'");
        lbl_SC_add.setBounds(x, y + 30, 200, 20);

        lbl_SC_edit = new JLabel("set Shortcut for 'EDIT'");
        lbl_SC_edit.setBounds(x + 200, y + 30, 200, 20);

        lbl_SC_settings = new JLabel("set Shortcut for 'SETTINGS'");
        lbl_SC_settings.setBounds(x + 400, y + 30, 200, 20);

        lbl_speed = new JLabel("set Speed of Graph");
        lbl_speed.setBounds(x, y + 100, 300, 20);

        btn_Done = new JButton("Done");
        btn_Done.setBackground(Color.blue);
        btn_Done.setForeground(Color.white);
        btn_Done.setBounds( x+600,  y+60, 100, 50);
        btn_Done.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                done = true;
            }
        });

        btn_background = new ColorChooserButton(backgroundColor);
        btn_background.setBounds(x + 140, y, 16, 16);

        btn_graphColor = new ColorChooserButton(this.graphColor);
        btn_graphColor.setBounds(x + 310, y, 16, 16);

        btn_SC_ADD = new KeyButton(shortcuts[0]);
        btn_SC_ADD.setBounds(x, y + 50, 110, 30);

        btn_SC_EDIT = new KeyButton(shortcuts[1]);
        btn_SC_EDIT.setBounds(x + 200, y + 50, 110, 30);

        btn_SC_SETTINGS = new KeyButton(shortcuts[2]);
        btn_SC_SETTINGS.setBounds(x + 400, y + 50, 110, 30);

        txt_Speed = new JTextField();
        txt_Speed.setBounds(x + 250, y + 100, 40, 20);


        add(lbl_background);
        add(lbl_graphColor);
        add(lbl_SC_add);
        add(lbl_SC_edit);
        add(lbl_SC_settings);
        add(lbl_speed);

        add(btn_background);
        add(btn_graphColor);
        add(btn_SC_ADD);
        add(btn_SC_EDIT);
        add(btn_SC_SETTINGS);
        add(txt_Speed);
        add(btn_Done);

        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setBackground(background);
        g.clearRect(0, 0, 1000, 1000);
    }

    public void activate() {
        setVisible(true);
        done=false;
    }

    public int getSpeed() {
        try {
            speed = Integer.parseInt(txt_Speed.getText());
        } catch (Exception e) {
        }
        return speed;
    }

    public char[] getShortcuts() {
        shortcuts[0] = btn_SC_ADD.getSelectedKey();
        shortcuts[1] = btn_SC_EDIT.getSelectedKey();
        shortcuts[2] = btn_SC_SETTINGS.getSelectedKey();

        return shortcuts;
    }

    public Color getGraphColor() {
        graphColor = btn_graphColor.getSelectedColor();
        return graphColor;
    }

    public Color getBackgroundColor() {
        background = btn_background.getSelectedColor();
        return background;
    }

    public boolean isDone() {
        return done;
    }
}
