package tools;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Vector {
    private String title;
    private Point pos;
    private short size, onTopOf;
    public static double t;
    private float w, angle;
    private Color c;
    private boolean visible;

    public Vector(String title, short len, float w, Color c) {
        this.title = title;
        this.w = w;
        this.c = c;
        this.size = len;

        pos = new Point();

    }

    public void tick() {
        pos.x = (int) (size * Math.sin(w * t));
        pos.y = (int) (size * Math.cos(w * t));
    }

    public void setW(float w) {
        this.w = w;
    }

    public float getW() {
        return w;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public short getS() {
        return size;
    }

    public void setSize(short size) {
        this.size = size;
    }

    public Point getPos() {
        return pos;
    }

    public void setPos(Point z) {
        pos = z;
    }

    public Color getColor() {
        return c;
    }

    public void setColor(Color c) {
        this.c = c;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public short getOnTopOf() {
        return onTopOf;
    }

    public void setOnTopOf(short onTopOf) {
        this.onTopOf = onTopOf;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
