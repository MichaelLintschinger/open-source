package tools;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyButton extends JButton {
    char selectedKey;
    boolean selection = false;

    public KeyButton(char in) {
        selectedKey=in;
        setText("Current: '" + selectedKey + "'");
        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pressed();
            }
        });
        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (selection) {
                    selection = false;
                    selectedKey = e.getKeyChar();
                }
                reset();
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
    }

    private void reset() {
        setText("Current: '" + selectedKey + "'");
    }

    private void pressed() {
        selection = true;
        setText("press a Key");
    }

    public char getSelectedKey(){
        return selectedKey;
    }
}
