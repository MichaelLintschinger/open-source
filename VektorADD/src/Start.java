import fps_Frame.FPSFrame;

public class Start {

    Start(){
        RenderPanel p=new RenderPanel();
        FPSFrame f = new FPSFrame("Vector",p,800,800,10);
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        f.start();
    }

    public static void main(String[] args){
        new Start();
    }
}
